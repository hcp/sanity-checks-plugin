
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.sanitychecks === 'undefined') {
	CCF.sanitychecks = { };
}

//CCF.sanitychecks.sanityCheckURI = undefined;
//CCF.sanitychecks.omID = undefined;
//CCF.sanitychecks.currentOverallStatus = undefined;
//CCF.sanitychecks.wait = undefined;
CCF.sanitychecks.projectConfigURI = window.location.protocol + "//" + window.location.host + "/xapi/sanityChecksSettings/" + window.projectScope + "/settings";

CCF.sanitychecks.acceptFail = function(scanno) {
	if (scanno==null) {
		alert("Accept session-level failure (SESSION=" + omID + ")");
	} else {
		alert("Accept scan-level failure (SESSION=" + omID + ", SCAN=" + scanno + ")");
	}
}

CCF.sanitychecks.markUnusable = function(scanno) {
	alert("Scan marked unusable (SESSION=" + omID + ", SCAN=" + scanno + ")");
}

CCF.sanitychecks.doShowBtn = function(str) {
	if (str.length>0) {
		$('#regSubmit').removeClass('disabled'); 
	} else {
		$('#regSubmit').addClass('disabled'); 
	}
}

//////////////////////////
//////////////////////////
//////////////////////////
// ACCEPT CHECK METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

CCF.sanitychecks.acceptThis = function(v) {

	<!-- (re)initialize registration form -->
	var failspan = "";
        var modal_contents;
	var scan_label;
	if (v != null) {
        	scan_label = $("#currentdiv_" + v).html().replace(/<\/*b>/g,'');
	} else {
        	scan_label = $("#currentdiv").html().replace(/<\/*b>/g,'');
	}
        scan_label = scan_label.substr(0,scan_label.indexOf("<"));
	if (v!=null) {
	   failspan = document.getElementById("failspan" + v).innerHTML;
	   failspan = failspan.replace(/[,\s]*$/,"");
	   $('#checkSpan').html(failspan);
           modal_contents =  
               '        <div id="action-div">' +
               '            <div>' +
               '            <p><strong>SCAN: </strong><span id="scanSpan" name="scanSpan">' + scan_label + '</span></p>' +
               '            </div>' +
               '            <div>' +
               '            <p><strong>FAILED CHECK(S): </strong><span id="checkSpan">' + $("#failspan" + v).html().replace(/, *$/,'') + '</span></p>' +
               '            </div>' +
               '            <div>' +
               '            <p><strong>JUSTIFICATION (Required): </strong>&nbsp;<br/><textarea id="justification" name="justification" onkeyup="CCF.sanitychecks.doShowBtn(this.value);" rows="4" cols="60"></textarea></p>' +
               '            </div>' +
               '        </div>'
               ;
	} else {
           modal_contents =  
               '        <div id="action-div">' +
               '            <div>' +
               '            <p><strong>SCAN: </strong><span id="scanSpan">Session Level Check</span></p>' +
               '            </div>' +
               '            <div>' +
               '            <p><strong>JUSTIFICATION (Required): </strong>&nbsp;<br/><textarea id="justification" name="justification" onkeyup="CCF.sanitychecks.doShowBtn(this.value);" rows="4" cols="60"></textarea></p>' +
               '            </div>' +
               '        </div>'
               ;
	}
        var action_modal_opts = {
            width: 560,
            height: 285,
            title: 'Accept Failed Sanity Check?',
            content: modal_contents,
            ok: 'show',
            okLabel: 'Accept',
            okAction: function(){
		CCF.sanitychecks.doAccept(v,document.getElementById('justification').value);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            }
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();

}
CCF.sanitychecks.acceptOK = function(){ 
	CCF.sanitychecks.getOverallStatus();
	xModalLoadingClose();
	if (CCF.sanitychecks.acceptCallback.scan!=null) {
		$('#currentdiv_' + CCF.sanitychecks.acceptCallback.scan).html(document.getElementById("accepteddiv_" + CCF.sanitychecks.acceptCallback.scan).innerHTML);
		$('#currentdiv2_' + CCF.sanitychecks.acceptCallback.scan).html(document.getElementById("accepteddiv2_" + CCF.sanitychecks.acceptCallback.scan).innerHTML);
	} else {
		$('#currentdiv').html(document.getElementById("accepteddiv").innerHTML);
	}
}
		
CCF.sanitychecks.acceptFailed = function(){
	xModalLoadingClose();
	xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

CCF.sanitychecks.acceptCallback = {
	scan : "X",
	success : CCF.sanitychecks.acceptOK,
	failure : CCF.sanitychecks.acceptFailed,
	scope : this
};

CCF.sanitychecks.doAccept = function(v,justification) {
	xModalLoadingOpen({title:'Please wait...'});
	if (v!=null) {
		var workflowURI = CCF.sanitychecks.sanityCheckURI + '/acceptScanChecks?XNAT_CSRF=' + csrfToken + '&scanList=' + v + '&justification=' + encodeURIComponent(justification);
		CCF.sanitychecks.acceptCallback.scan=v;
	} else {
		var workflowURI = CCF.sanitychecks.sanityCheckURI + '/acceptSessionChecks?XNAT_CSRF=' + csrfToken + '&justification=' + encodeURIComponent(justification);
		CCF.sanitychecks.acceptCallback.scan=null;
	}
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), CCF.sanitychecks.acceptCallback, null, this);
}


//////////////////////////
//////////////////////////
//////////////////////////
// ACCEPT ALL METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

CCF.sanitychecks.acceptAll = function() {

	<!-- (re)initialize registration form -->
	var modal_contents = 
            '<div id="action-all-div">' +
            '    <div class="inputWrapper">' +
            '    <p><strong>ACCEPT ALL CHECKS ON PAGE????</strong></p>' +
            '    </div>' +
            '    <div class="inputWrapper">' +
            '    <p><strong>JUSTIFICATION (Required): </strong>&nbsp;<br/><textarea id="justification" name="justification" onkeyup="CCF.sanitychecks.doShowBtn(this.value);" rows="4" cols="60"></textarea></p>' +
            '    </div>' +
            '</div>' 
	;
        var action_modal_opts = {
            width: 560,
            height: 285,
            title: 'Accept Failed Sanity Checks?',
            content: modal_contents,
            ok: 'show',
            okLabel: 'Accept',
            okAction: function(){
		CCF.sanitychecks.doAcceptAll(document.getElementById('justification').value);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            },
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();
	$('#action_modal .footer').css('background-color','#ffffff');

}
CCF.sanitychecks.acceptAllOK = function(){ 
	window.location.reload();
}
		
CCF.sanitychecks.acceptAllFailed = function(){
	xModalLoadingClose();
	xModalMessage('Error','This functionality is not yet implemented.','OK', null);
	//xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

CCF.sanitychecks.acceptAllCallback = {
	success : CCF.sanitychecks.acceptAllOK,
	failure : CCF.sanitychecks.acceptAllFailed,
	scope : this
};

CCF.sanitychecks.doAcceptAll = function(justification) {
	xModalLoadingOpen({title:'Please wait...'});
	var workflowURI = CCF.sanitychecks.sanityCheckURI + '/acceptAllChecks?XNAT_CSRF=' + csrfToken + '&justification=' + encodeURIComponent(justification);
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), CCF.sanitychecks.acceptAllCallback, null, this);
}

///////////////////////////
///////////////////////////
///////////////////////////
// MARK UNUSABLE METHODS //
///////////////////////////
///////////////////////////
///////////////////////////

CCF.sanitychecks.markThisUnusable = function(v) {

	<!-- (re)initialize registration form -->
	var failspan = "";
        var modal_contents;
	if (v!=null) {
	   failspan = document.getElementById("failspan" + v).innerHTML;
	   failspan = failspan.replace(/[,\s]*$/,"");
	   $('#checkSpan').html(failspan);
           modal_contents =  
               '        <div id="action-div">' +
               '            <div>' +
               '            <p><strong>SCAN: </strong><span id="scanSpan" name="scanSpan">' + scan_label + '</span></p>' +
               '            </div>' +
               '            <div>' +
               '            <p><strong>FAILED CHECK(S): </strong><span id="checkSpan">' + $("#failspan" + v).html().replace(/, *$/,'') + '</span></p>' +
               '            </div>' +
               '            <div>' +
               '            <p><strong>JUSTIFICATION (Required): </strong>&nbsp;<br/><textarea id="justification" name="justification" onkeyup="CCF.sanitychecks.doShowBtn(this.value);" rows="4" cols="60"></textarea></p>' +
               '            </div>' +
               '        </div>'
               ;
	} else {
 	   // Not applicable
 	   return;
	}
        var scan_label = $("#currentdiv_" + v).html().replace(/<\/*b>/g,'');
        scan_label = scan_label.substr(0,scan_label.indexOf("<"));
	
        var action_modal_opts = {
            width: 560,
            height: 285,
            title: 'Mark scan unusable?',
            content: modal_contents,
            ok: 'show',
            okLabel: 'Mark Unusable',
            okAction: function(){
		CCF.sanitychecks.doMarkUnusable(v);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            },
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();
	$('#action_modal .footer').css('background-color','#ffffff');

}
CCF.sanitychecks.markUnusableOK = function(){ 
	xModalLoadingClose();
	if (CCF.sanitychecks.markUnusableCallback.scan!=null) {
		$('#currentdiv_' + CCF.sanitychecks.markUnusableCallback.scan).html(document.getElementById("unusablediv_" + CCF.sanitychecks.markUnusableCallback.scan).innerHTML);
		$('#currentdiv2_' + CCF.sanitychecks.markUnusableCallback.scan).html(document.getElementById("unusablediv2_" + CCF.sanitychecks.markUnusableCallback.scan).innerHTML);
	} 
	CCF.sanitychecks.getOverallStatus();
}
		
CCF.sanitychecks.markUnusableFailed = function(){
	CCF.sanitychecks.getOverallStatus();
	xModalLoadingClose();
	xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

CCF.sanitychecks.markUnusableCallback = {
	scan : "X",
	success : CCF.sanitychecks.markUnusableOK,
	failure : CCF.sanitychecks.markUnusableFailed,
	scope : this
};

CCF.sanitychecks.doMarkUnusable = function(v) {
	xModalLoadingOpen({title:'Please wait...'});
	if (v!=null) {
		var workflowURI = CCF.sanitychecks.sanityCheckURI + '/markScansUnusable?XNAT_CSRF=' + csrfToken + '&scanList=' + v;
		CCF.sanitychecks.markUnusableCallback.scan=v;
		YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), CCF.sanitychecks.markUnusableCallback, null, this);
	} 
}

///////////////////////
///////////////////////
///////////////////////
// SHOW LINK METHODS //
///////////////////////
///////////////////////
///////////////////////

CCF.sanitychecks.checkShowLink = function(toUri) {
	CCF.sanitychecks.wait = true;
	CCF.sanitychecks.checkShowLinkCallback.toUri = toUri;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(CCF.sanitychecks.projectConfigURI), CCF.sanitychecks.checkShowLinkCallback, null, this);
}

CCF.sanitychecks.checkShowLinkOK = function(o){ 
	var config = JSON.parse(o.responseText);
	//console.log("DEBUG(2)", XNAT.data.context);
	//console.log("DEBUG(3) config=",config);
	if (typeof config.enabled == 'undefined' || !config.enabled) {
		$(".sanityCheckLink").css("display","none");
	} else {
		$(".sanityCheckLink").css("display","inline");
	} 
}

CCF.sanitychecks.checkShowLinkFailed = function(o){ 
	$(".sanityCheckLink").css("display","none");
}

CCF.sanitychecks.checkShowLinkCallback = {
	success : CCF.sanitychecks.checkShowLinkOK,
	failure : CCF.sanitychecks.checkShowLinkFailed,
	scope : this
};

//////////////////////////
//////////////////////////
//////////////////////////
// RERUN CHECKS METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

CCF.sanitychecks.rerunChecks = function(toUri) {
	CCF.sanitychecks.wait = true;
	CCF.sanitychecks.projectCheckCallback.toUri = toUri;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(CCF.sanitychecks.projectConfigURI), CCF.sanitychecks.projectCheckCallback, null, this);
}

CCF.sanitychecks.ckRun = function(config, toUri) {
		
		this.confirm = xModalConfirm;
		this.confirm.okAction = function(){ 
			CCF.sanitychecks.xModalRunningOpen();
			var workflowURI = (config.checkerType === "AUTOMATION_SCRIPT") ? CCF.sanitychecks.sanityCheckURI + '/runSanityChecks?XNAT_CSRF=' + csrfToken :
				serverRoot + "/xapi/projects/" + XNAT.data.context.projectID + 
				((typeof XNAT.data.context.subjectID !== "undefined" && XNAT.data.context.subjectID.length>0) ?  "/subjects/" + XNAT.data.context.subjectID : "") +
				 "/experiments/" + XNAT.data.context.ID +
					 "/runJavaBasedChecks?XNAT_CSRF=" + csrfToken 
			;
			YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), CCF.sanitychecks.rerunCallback, null, this);
			CCF.sanitychecks.projectCheckCallback.toUri = toUri;
		 }
		this.confirm.content  = '<p>Run sanity checks?</p>';
		this.confirm.cancelAction = function(){ return; };
		this.confirm.width = 260;
		this.confirm.height = 132;
		xModalOpenNew(this.confirm);

}

CCF.sanitychecks.checkOK = function(o){ 
	var config = JSON.parse(o.responseText);
	if (typeof config.enabled !== 'undefined' && config.enabled) {
		CCF.sanitychecks.ckRun(config, CCF.sanitychecks.projectCheckCallback.toUri);
		return;
	} 
	xModalMessage('Error','Sanity checks have not been enabled for this project.','OK', null);
}
CCF.sanitychecks.checkFailed = function(o){ 
	CCF.sanitychecks.ckRun(CCF.sanitychecks.projectCheckCallback.toUri);
}

CCF.sanitychecks.projectCheckCallback = {
	success : CCF.sanitychecks.checkOK,
	failure : CCF.sanitychecks.checkFailed,
	scope : this
};

CCF.sanitychecks.rerunOK = function(o){ 
	if (CCF.sanitychecks.wait) {
		if (CCF.sanitychecks.projectCheckCallback.toUri != undefined) {
			window.location.assign(window.location.protocol + "//" + window.location.host + CCF.sanitychecks.projectCheckCallback.toUri);
		} else {
			window.location.reload();
		}
	}
}
		
CCF.sanitychecks.rerunFailed = function(o){
	if (CCF.sanitychecks.wait) {
		if (o.status == 400 && typeof o.responseText !== 'undefined' && o.responseText.length>0) {
			xModalMessage('Error',o.responseText, 'OK', null);
		} else {
			xModalMessage('Error','An unexpected error has occured. Please contact your administrator.' +
				 "<br/><br/>   STATUS:  " + o.status + "<br/><br/>   " + o.responseText,'OK', null);
		}
		xModalCloseNew("runningModal",'');
		CCF.sanitychecks.wait = false;
	}
}

CCF.sanitychecks.rerunCallback = {
	success : CCF.sanitychecks.rerunOK,
	failure : CCF.sanitychecks.rerunFailed,
	scope : this
};


///////////////////////////
///////////////////////////
///////////////////////////
// REQUEST EMAIL METHODS //
///////////////////////////
///////////////////////////
///////////////////////////

CCF.sanitychecks.requestEmail = function() {
	CCF.sanitychecks.wait = false;
	var emailURI = CCF.sanitychecks.sanityCheckURI + '/emailOnRun?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(emailURI), CCF.sanitychecks.requestEmailCallback, null, this);
}
CCF.sanitychecks.requestEmailOK = function(o){ 
	alert("You will be sent an e-mail upon completion");
}
		
CCF.sanitychecks.requestEmailFailed = function(o){ }

CCF.sanitychecks.requestEmailCallback = {
	success : CCF.sanitychecks.requestEmailOK,
	failure : CCF.sanitychecks.requestEmailFailed,
	scope : this
};

////////////////////////////
////////////////////////////
////////////////////////////
// OVERALL STATUS METHODS //
////////////////////////////
////////////////////////////
////////////////////////////


CCF.sanitychecks.updateModal = {
        id: 'updateModal',
        kind: 'fixed',  // REQUIRED - options: 'dialog','fixed','large','med','small','xsmall','custom'
        width: 420, // width in px - used for 'fixed','custom','static'
        height: 220, // height in px - used for 'fixed','custom','static'
        scroll: 'yes', // true/false - does content need to scroll?
        title: 'Message', // text for title bar
	content: "WARNING:  The overall status appears to be out of date (this can occur, for example, if a failed scan has been set to unusable outside this report). <br/></br><b>Update Overal Status?</b>",
        // footer not specified, will use default footer
        ok: 'show',  // show the 'ok' button
        okLabel: 'Update Overall Status',
        okAction: function(){ CCF.sanitychecks.updateOverallStatus();},
        cancel: 'show' // do NOT show the 'Cancel' button
};

CCF.sanitychecks.calculateStatusOK = function(o){ 
	if (o.responseText!=CCF.sanitychecks.currentOverallStatus) {
	    xModalOpenNew(CCF.sanitychecks.updateModal);
	}
}
		 
CCF.sanitychecks.calculateStatusFailed = function(){
	// Do nothing for now
}

CCF.sanitychecks.calculateStatusCallback = {
	success : function(o){ CCF.sanitychecks.calculateStatusOK(o) },
	failure : CCF.sanitychecks.calculateStatusFailed,
	scope : this
};

CCF.sanitychecks.calculateOverallStatus = function() {
	var workflowURI = CCF.sanitychecks.sanityCheckURI + '/calculateOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(workflowURI), CCF.sanitychecks.calculateStatusCallback, null, this);
}

CCF.sanitychecks.updateStatusOK = function(o){ 
	CCF.sanitychecks.getOverallStatus();
}
		
CCF.sanitychecks.updateStatusFailed = function(){
	xModalMessage('Warning','Couldn\'t update overall status.','OK', null);
}

CCF.sanitychecks.updateStatusCallback = {
	success : function(o){ CCF.sanitychecks.updateStatusOK(o) },
	failure : CCF.sanitychecks.updateStatusFailed,
	scope : this
};

CCF.sanitychecks.updateOverallStatus = function() {
	var workflowURI = CCF.sanitychecks.sanityCheckURI + '/updateOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), CCF.sanitychecks.updateStatusCallback, null, this);
}

CCF.sanitychecks.overallStatusOK = function(o){ 
	CCF.sanitychecks.currentOverallStatus = o.responseText;
	$('#overallStatusSpan').html('<font class="status field' + o.responseText + '">' + o.responseText + "</font>");
	CCF.sanitychecks.calculateOverallStatus();
}
		
CCF.sanitychecks.overallStatusFailed = function(){
	xModalMessage('Warning','Couldn\'t update overall status.','OK', null);
}

CCF.sanitychecks.overallStatusCallback = {
	success : function(o){ CCF.sanitychecks.overallStatusOK(o) },
	failure : CCF.sanitychecks.overallStatusFailed,
	scope : this
};

CCF.sanitychecks.getOverallStatus = function() {
	var workflowURI = CCF.sanitychecks.sanityCheckURI + '/getOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(workflowURI), CCF.sanitychecks.overallStatusCallback, null, this);
}

/////////////////////
/////////////////////
/////////////////////
// UTILITY METHODS //
/////////////////////
/////////////////////
/////////////////////

CCF.sanitychecks.xModalRunningOpen = function(){

	runningModal = {
	    id: 'runningModal',
	    kind: 'fixed',
	    width: 260,
	    height: 132,
	    title: 'Running checks',
	    content: '<img src="'+serverRoot+'/images/loading_bar.gif" alt="loading">',
	    ok: 'show',
	    okLabel: 'Don\'t Wait',
	    okAction: function(){CCF.sanitychecks.wait = false;CCF.sanitychecks.requestEmail();},
	    okClose: 'yes',
	    cancel: 'hide'
		};
	xModalOpenNew(runningModal);

}

