
//# sourceURL=sanity-checks-plugin/sanityChecksSettings.js

if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.scsettings === 'undefined') {
	CCF.scsettings = { };
}

CCF.scsettings.initialize = function() {

	var scConfigAjax = $.ajax({
		type : "GET",
 		url:serverRoot+'/data/automation/scripts?format=json',
		cache: false,
		async: true,
		context: this,
		dataType: 'json'
	 });
	scConfigAjax.done( function( data, textStatus, jqXHR ) {
		if (typeof data !== 'undefined') {
			CCF.scsettings.automationScripts = data;
		} else {
			CCF.scsettings.automationScripts = {};
		} 
		CCF.scsettings.continueInit();
	});
	scConfigAjax.fail( function( data, textStatus, error ) {
		CCF.scsettings.automationScripts = {};
		CCF.scsettings.continueInit();
	});

}

CCF.scsettings.continueInit = function() {

	var scConfigAjax = $.ajax({
		type : "GET",
 		url:serverRoot+'/xapi/sanityChecksSettings/checksClasses',
		cache: false,
		async: true,
		context: this,
		dataType: 'json'
	 });
	scConfigAjax.done( function( data, textStatus, jqXHR ) {
		CCF.scsettings.checksClasses = data;
		CCF.scsettings.continueInitPopulateUI();
	});
	scConfigAjax.fail( function( data, textStatus, error ) {
		CCF.scsettings.checksClasses = {};
		CCF.scsettings.continueInitPopulateUI();
	});


}

CCF.scsettings.continueInitPopulateUI = function() {

	CCF.scsettings.classOptions = { }
	CCF.scsettings.scriptOptions = { }
	var checksClasses = CCF.scsettings.checksClasses;
	checksClasses.sort(function(x,y) {
		var xRepl = x.replace(/^.*[.]/,"");
		var yRepl = y.replace(/^.*[.]/,"");
		return xRepl.localeCompare(yRepl); 
	});
	//CCF.scsettings.classOptions['']='None';
	for (var i=0; i<checksClasses.length; i++) {
		var clazz = checksClasses[i];
		var clazzName = clazz.replace(/^.*[.]/,"");
		CCF.scsettings.classOptions[clazz]=clazzName;
	} 
	var classObjs = CCF.scsettings.automationScripts["ResultSet"]["Result"];
	//CCF.scsettings.scriptOptions['']='None';
	classObjs.sort(function(x,y) {
		var varX = x["Script ID"];
		var varY = y["Script ID"];
		return varX.localeCompare(varY); 
	})
	for (var i=0; i<classObjs.length; i++) {
		var key = classObjs[i]["Script ID"];
		CCF.scsettings.scriptOptions[key]=key.replace(/^.*[.]/,"");
	} 

	var viewInfo = {
		viewPanel: {
			kind: 'panel.form',
			method: 'POST',
			group: 'sanityChecksPlugin',
			url: '/xapi/sanityChecksSettings/' + XNAT.data.projectId + '/settings' ,
			contentType: 'json',
			name: 'sanityChecksSettings',
			label: 'SanityChecks Settings',
			onload: function() {
					$("#checkerType").change(function() {
						CCF.scsettings.showHideType();
					});
					CCF.scsettings.showHideType();
			},
			contents: {
				//message: {
				//	tag: 'div.message.bold',
				//	content: 'NONE'
				//},
				enabled: {
					kind: 'panel.input.switchbox',
					id: 'enabled',
					name: 'enabled',
					value: true,
					className: 'required srconfig-ele',
					label: 'SanityChecks Enabled?',
					description: 'Enables/Disables sanity checks functionality for this project.',
					onText: 'Enabled',
					offText: 'Disabled'
				},
				checkerType: {
					kind: 'panel.select.single',
					id: 'checkerType',
					name: 'checkerType',
					className: 'required srconfig-ele',
					label: 'Checker Type',
					options: {
						'JAVA_CLASS':'Java Class',
						'AUTOMATION_SCRIPT':'Automation Script (DEPRECATED)'
					}
				},
				checksClass: {
					kind: 'panel.select.single',
					id: 'checksClass',
					name: 'checksClass',
					className: 'required srconfig-ele',
					label: 'Checks Class',
					description: 'An @SanityChecker annotated class supplied in a plugin that performs sanity checks',
					options: CCF.scsettings.classOptions
				},
				scriptId: {
					kind: 'panel.select.single',
					id: 'scriptId',
					name: 'scriptId',
					className: 'required srconfig-ele',
					label: 'Script ID (Deprecated)',
					description: 'An automation script that performs sanity checks<br><br>' +
						'NOTE:  The sanity checks script will receive the folloing parameters from the script runner: ' +
						'<b>projectId, subjectLbl and experimentLbl</b>.' +  
						'It should post a sanity checks assessor (<b>san:sanityChecks</b>) to the experiment upon completion.',
					options: CCF.scsettings.scriptOptions
				}
			}
		}
	};
	$("#sanityChecksSettingsDiv").html("");
	XNAT.spawner.spawn(viewInfo).render($("#sanityChecksSettingsDiv")[0]);
}

CCF.scsettings.showHideType = function() {
	var checkerType = $("#checkerType").val();
	console.log('checkerType=',checkerType);
	console.log('typeof checkerType=',typeof checkerType);
	if (!checkerType) {
		$('[data-name=checksClass]').prop('disabled', true);
		$('[data-name=scriptId]').prop('disabled', true);
		$('[data-name=checksClass]').hide();
		$('[data-name=scriptId]').hide();
	} else if (checkerType != 'AUTOMATION_SCRIPT') {
		$('[data-name=checksClass]').prop('disabled', false);
		$('[data-name=scriptId]').prop('disabled', true);
		$('[data-name=checksClass]').show();
		$('[data-name=scriptId]').hide();
	} else {
		$('[data-name=checksClass]').prop('disabled', true);
		$('[data-name=scriptId]').prop('disabled', false);
		$('[data-name=checksClass]').hide();
		$('[data-name=scriptId]').show();
	}
}

/*
CCF.scsettings.updateConfiguration = function() {
	var config = {};
	config.enabled = ($('#CB_useSanityChecks').is(':checked')) ? 1 : 0;
	config.scriptID = $('#SL_sanityCheckScript').val();
	if (config.enabled == 1 && (typeof config.scriptID == 'undefined' || config.scriptID == '')) {
		xmodal.message('Error','ERROR:  A valid script must be selected in order to enable sanity checks');
		return;
	}
	CCF.scsettings.settings = config;
	var scConfigAjax = $.ajax({
		type : "PUT",
 		url:serverRoot+'/data/projects/' + XNAT.data.context.project +'/config/sanity_checks/configuration?inbody=true&XNAT_CSRF=' + window.csrfToken,
		cache: false,
		async: false,
		data:  JSON.stringify(CCF.scsettings.settings),
		contentType: "application/text; charset=utf-8"
	 });
	scConfigAjax.done( function( data, textStatus, jqXHR ) {
		xmodal.message('Saved','The sanity checks configuration has been saved');
	});
	scConfigAjax.fail( function( data, textStatus, error ) {
		xmodal.message('Error','ERROR:  Configuration could not be saved');
	});
}
*/



