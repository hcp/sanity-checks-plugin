
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.sanitychecksreport === 'undefined') {
	CCF.sanitychecksreport = { };
}


if (typeof CCF.sanitychecksreport.project === 'undefined' || CCF.sanitychecksreport.project == "") {
   var queryParams = new URLSearchParams(window.location.search);
   CCF.sanitychecksreport.project = queryParams.get("project");
}

CCF.sanitychecksreport.initFailureReportTable = function() {

	$("a[href='#failure-tab']").click(function() {
		setTimeout(function(){
			CCF.sanitychecksreport.populateRowCount();
		},10);
	});

  $('#scReport-contents').html("<div id='sc-building-div'><img src='"+serverRoot+"/images/loading_spinner.gif'/> Building Report.</div>" +
         "<div id='scFailureReport-table' style='width:100%;max-height:700px;overflow-x:auto;overflow-y:auto'></div>" +
         "<div id='screports-row-count-container''></div>");

    var columnVar = 
           {
              assessor: {
                label: 'Assessor',
                filter: true,
            	td: {'className': 'assessor center rowcount'},
                apply: function() {
                  return "<a href='/data/experiments/" + this.ID + "?format=html' target='_blank'>" + this.label + "</a>";
                },
                sort: true
              },
              imageSessionDate: {
                label: 'SessionDate',
                filter: true,
            	td: {'imageSessionDate': 'imageSessionDate center'},
                sort: true
              },
              site: {
                label: 'Site',
                filter: true,
            	td: {'className': 'site center'},
                sort: true
              },
              scanner: {
                label: 'Scanner',
                filter: true,
            	td: {'className': 'scanner center'},
                sort: true
              },
              operator: {
                label: 'Operator',
                filter: true,
            	td: {'className': 'operator center'},
                sort: true
              },
              scanID: {
                label: 'Scan',
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'ALL', value: 'all' },
                       { label: 'EMPTY', value: 'empty' },
                       { label: 'NOT EMPTY', value: 'not_empty' }
                     ],
                     element: filterMenuElement.call(table, 'scanID')
                   }).element])
                 },
            	td: {'className': 'scanID center'},
                sort: true
              },
              scanType: {
                label: 'scanType',
                filter: true,
            	td: {'className': 'scanType center'},
                sort: true
              },
              seriesDesc: {
                label: 'SeriesDesc',
                filter: true,
            	td: {'className': 'seriesDesc center'},
                sort: true
              },
              checkType: {
                label: 'CheckType',
            	td: {'className': 'checkType center'},
                filter: function(table) {
                   return spawn('div.center', [XNAT.ui.select.menu({
                     value: 'all',
                     options: [
                       { label: 'ALL', value: 'all' },
                       { label: 'LINKED_DATA', value: 'LINKED_DATA' },
                       { label: 'VALIDATION', value: 'VALIDATION' },
                       { label: 'PIPELINE', value: 'PIPELINE' },
                       { label: 'COMPLETION', value: 'COMPLETION' },
                       { label: 'OTHER', value: 'OTHER' }
                     ],
                     element: filterMenuElement.call(table, 'checkType')
                   }).element])
                 },
                sort: true
              },
              checkID: {
                label: 'CheckID',
                filter: true,
            	td: {'className': 'checkID center'},
                sort: true
              },
              checkDescription: {
                label: 'Description',
                filter: true,
            	td: {'className': 'checkDescription center', 'style': 'min-width:220px;max-width:440px;'},
                sort: true
              },
              checkDiagnosis: {
                label: 'Diagnosis',
                filter: true,
            	td: {'className': 'checkDiagnosis center', 'style': 'min-width:280px;max-width:560px'},
                sort: true
              }

           }
       ;

    // set up custom filter menus
    function filterMenuElement(prop){
      //console.log(prop);
      if (!prop) return false;
      // call this function in context of the table
      var $pipelineTable = $(this);
      //console.log($pipelineTable);
      var FILTERCLASS = 'filter-' + prop;
      //console.log(FILTERCLASS);
      return {
        id: 'pcp-filter-select-' + prop,
        on: {
          change: function() {
            var selectedValue = $(this).val();
            $("td." + prop).each(function() {
              if ($(this).hasClass("filter")) {
                  return true;
              }
              var $row = $(this).closest('tr');
               //console.log($row)
              if (selectedValue === 'all') {
                $row.removeClass(FILTERCLASS);
                return;
              }
    
              $row.addClass(FILTERCLASS);
              if (selectedValue === 'empty') {
		if (this.textContent == "") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (selectedValue === 'not_empty') {
		if (this.textContent != "") {
                	$row.removeClass(FILTERCLASS);
		}
              } else if (this.textContent.includes(selectedValue)) {
                $row.removeClass(FILTERCLASS);
              }
            })
    
    	CCF.sanitychecksreport.populateRowCount();
    
          }
        }
      };
    }


 setTimeout(function(){
        $("li[data-tab='qc-summary-tab']").removeClass("active");
        $("#sanity-checks-group").find("li").removeClass("active");
        $("li[data-tab='qc-summary-tab']").addClass("active");
  },100)
  XNAT.xhr.getJSON({
     url: '/xapi/sanityChecksReports/project/' + CCF.sanitychecksreport.project +
          '/failureReport',
     success: function(data) {
	CCF.sanitychecksreport.failureData = data;
        XNAT.table.dataTable(data, {
           table: {
              id: 'sc-qc-datatable',
              className: 'sc-qc-table highlight'
           },
           width: 'fit-content',
           overflowY: 'hidden',
           overflowX: 'hidden',
           columns: columnVar
        }).render($('#scFailureReport-table'))
        $("#scFailureReport-table").find(".data-table-wrapper").css("display","table");
     	$("#sc-building-div").html("<div style='width:100%;text-align:right'><a id='sc-download-csv' download='SanityCheckReport.csv' onclick='CCF.sanitychecksreport.doDownload()' type='text/csv'>Download CSV</a></div>");
        setTimeout(function(){
        	CCF.sanitychecksreport.populateRowCount();
        },100);
        $('input.filter-data').keyup(function() {
            setTimeout(function(){
               CCF.sanitychecksreport.populateRowCount();
           },50)
        });
/*
        setTimeout(function(){
    		$('.highlight').on("click",function() {
			$(this).closest('tr').find('td').each(function(){
				if ($(this).css("background-color").toString().indexOf('238')>=0) {
					$(this).css("background-color","");
				} else {
					$(this).css("background-color","rgb(238,238,238)");
				}
			});
		});
        },100);
*/
     },
     failure: function(data) {
     	$("#sc-building-div").html("<h3>Failed to build report</h3>");
     }
  });

}

CCF.sanitychecksreport.doDownload = function() {
	var csv = CCF.sanitychecksreport.generateCSV();
	var data = new Blob([csv]);
	var ele = document.getElementById("sc-download-csv");
	ele.href = URL.createObjectURL(data);
}

CCF.sanitychecksreport.generateCSV = function() {

    var dta = CCF.sanitychecksreport.failureData;
    var result = "Assessor,SessionDate,Site,Scanner,Operator,ScanID,ScanType,SeriesDesc,CheckType,CheckID,Description,Diagnosis\n";
    dta.forEach(function(obj){
	if (obj.hasOwnProperty("label") && typeof obj["label"] !== 'undefined' && obj["label"]) {
        	result += ((obj["label"].indexOf(",")<0) ? obj["label"] : "\"" + obj["label"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("imageSessionDate") && typeof obj["imageSessionDate"] !== 'undefined' && obj["imageSessionDate"]) {
        	result += ((obj["imageSessionDate"].indexOf(",")<0) ? obj["imageSessionDate"] : "\"" + obj["imageSessionDate"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("site") && typeof obj["site"] !== 'undefined' && obj["site"]) {
        	result += ((obj["site"].indexOf(",")<0) ? obj["site"] : "\"" + obj["site"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("scanner") && typeof obj["scanner"] !== 'undefined' && obj["scanner"]) {
        	result += ((obj["scanner"].indexOf(",")<0) ? obj["scanner"] : "\"" + obj["scanner"].replace('"','\\"') + "\"");
	}
        result += ",";
	if (obj.hasOwnProperty("operator") && typeof obj["operator"] !== 'undefined' && obj["operator"]) {
        	result += ((obj["operator"].indexOf(",")<0) ? obj["operator"] : "\"" + obj["operator"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("scanID") && typeof obj["scanID"] !== 'undefined' && obj["scanID"]) {
        	result += ((obj["scanID"].indexOf(",")<0) ? obj["scanID"] : "\"" + obj["scanID"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("scanType") && typeof obj["scanType"] !== 'undefined' && obj["scanType"]) {
        	result += ((obj["scanType"].indexOf(",")<0) ? obj["scanType"] : "\"" + obj["scanType"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("seriesDesc") && typeof obj["seriesDesc"] !== 'undefined' && obj["seriesDesc"]) {
        	result += ((obj["seriesDesc"].indexOf(",")<0) ? obj["seriesDesc"] : "\"" + obj["seriesDesc"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("checkType") && typeof obj["checkType"] !== 'undefined' && obj["checkType"]) {
        	result += ((obj["checkType"].indexOf(",")<0) ? obj["checkType"] : "\"" + obj["checkType"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("checkID") && typeof obj["checkID"] !== 'undefined' && obj["checkID"]) {
        	result += ((obj["checkID"].indexOf(",")<0) ? obj["checkID"] : "\"" + obj["checkID"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("checkDescription") && typeof obj["checkDescription"] !== 'undefined' && obj["checkDescription"]) {
        	result += ((obj["checkDescription"].indexOf(",")<0) ? obj["checkDescription"] : "\"" + obj["checkDescription"].replace('"','\\"') + "\"");
	} 
        result += ",";
	if (obj.hasOwnProperty("checkDiagnosis") && typeof obj["checkDiagnosis"] !== 'undefined' && obj["checkDiagnosis"]) {
        	result += ((obj["checkDiagnosis"].indexOf(",")<0) ? obj["checkDiagnosis"] : "\"" + obj["checkDiagnosis"].replace('"','\\"') + "\"");
	} 
        result += "\n";
    });

    //var keys = Object.keys(dta[0]);

    //var result = keys.join(",") + "\n";

    //dta.forEach(function(obj){
    //    keys.forEach(function(k, ix){
    //        if (ix) result += ",";
    //        result += ((obj[k].indexOf(",")<0) ? obj[k] : "\"" + obj[k].replace('"','\\"') + "\"");
    //    });
    //    result += "\n";
    //});

    return result;

}


CCF.sanitychecksreport.populateRowCount = function() {
   var rowCount = ($('.rowcount').closest('tr').filter(":visible")).length;
    if ($("#sc-building-div").find('img').length<1) {
    	$('#screports-row-count-container').html("<em>" + rowCount + " rows match query filters.</em>");
    } else {
    	$('#screports-row-count-container').empty();
    }
}


