package org.nrg.ccf.sanitychecks.abst;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckRemoverI;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.xdat.model.SanSanitychecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;

import com.google.common.collect.Lists;

public abstract class AbstractSanityChecker implements SanityCheckerI, SanityCheckRemoverI {
	
	final List<String> _scanChecksToRemove = new ArrayList<>();
	final List<String> _sessionChecksToRemove = new ArrayList<>();
	
	@Override
	public boolean hasChecksToRemove() {
		return _scanChecksToRemove.size()>0 || _sessionChecksToRemove.size()>0;
	}

	@Override
	public void removeChecks(SanSanitychecks sanityChecks, UserI user) throws SanityChecksException {
		boolean modified = false;
		if (_sessionChecksToRemove.size()>0) {
			for (final String check : _sessionChecksToRemove) {
				final List<SanSanitychecksCheckI> sessionChecks = sanityChecks.getSession_sessionChecks_check();
				for (int i=0; i<sessionChecks.size(); i++) {
					final SanSanitychecksCheckI sanCheck = sessionChecks.get(i);
					if (sanCheck.getId().equals(check)) {
						sanityChecks.removeSession_sessionChecks_check(i);
						modified = true;
					}
				}
			}
		}
		if (_scanChecksToRemove.size()>0) {
			for (final String check : _scanChecksToRemove) {
				// Unfortunately here, we need to remove all the checks for the scan if we need to remove one.
				final List<SanSanitychecksScanChecksI> scans = sanityChecks.getScans_scanChecks();
				final List<Integer> removeList = new ArrayList<>();
				for (int i=0; i<scans.size(); i++) {
					final SanSanitychecksScanChecksI scan = scans.get(i);
					// Let's not mess with accepted scan checks.
					if (scan.getStatus().equalsIgnoreCase(Outcome.ACCEPTED.toString())) {
						continue;
					}
					final List<SanSanitychecksScanChecksCheckI> scanChecks = scan.getCheck();
					for (int j=0; j<scanChecks.size(); j++) {
						final SanSanitychecksScanChecksCheckI scanCheck = scanChecks.get(j);
						if (scanCheck.getId().equals(check)) {
							removeList.add(i);
							break;
						}
					}
				}
				if (removeList.size()>0) {
					modified = true;
					for (Integer i : Lists.reverse(removeList)) {
						sanityChecks.removeScans_scanChecks(i);
					}
				}
			}
		}
		if (modified) {
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, sanityChecks.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(sanityChecks,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				// Do nothing for now
			}
		}
	}
	
	public void addScanChecksToRemove(List<String> checkList) {
		_scanChecksToRemove.addAll(checkList);
	}
	
	public List<String> getScanChecksToRemove() {
		return _scanChecksToRemove;
	}
	
	public void addSessionChecksToRemove(List<String> checkList) {
		_sessionChecksToRemove.addAll(checkList);
	}
	
	public List<String> getSessionChecksToRemove() {
		return _sessionChecksToRemove;
	}

}
