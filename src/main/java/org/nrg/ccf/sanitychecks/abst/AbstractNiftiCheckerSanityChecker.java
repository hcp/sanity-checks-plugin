package org.nrg.ccf.sanitychecks.abst;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.CcfNiftiUtils;
import org.nrg.ccf.common.utilities.utils.ResourceCheckUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.sanitychecks.constants.CheckResult;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractNiftiCheckerSanityChecker extends AbstractSanityChecker {
	
	public static final String NIFTI_CHECK_SHORT_NAME = "NIFTI_CHECK";
	public static final String NIFTI_CHECK_LONG_NAME = "NIFTI files generated?";
	public static final String NIFTI_FILE_CHECKS_SHORT_NAME = "NIFTI_FILE_CHECKS";
	public static final String NIFTI_FILE_CHECKS_LONG_NAME = "NIFTI size/naming/fileCounts/date OK?";
	public static final String INITIALFRAMES_NIFTI_CHECK_SHORT_NAME = "NIFTI InitialFrames CHECK";
	public static final String INITIALFRAMES_NIFTI_CHECK_LONG_NAME = "NIFTI InitialFrames file generated?";
	public static final String NIFTI_FRAMECOUNT_CHECK_SHORT_NAME = "NIFTI FrameCount CHECK";
	public static final String NIFTI_FRAMECOUNT_CHECK_LONG_NAME = "NIFTI frame count correct?";
	public static final String BIDSJSON_CHECK_SHORT_NAME = "BIDSJSON_CHECK";
	public static final String BIDSJSON_CHECK_LONG_NAME = "BIDS JSON sidecar file generated and contains content?";
	protected final Map<String, List<String>> _initialFramesTypeMap = new HashMap<>();
	protected final Map<String, List<String>> _initialFramesDescMap = new HashMap<>();
	// Skipping the FrameCount check for scouts.  We don't distribute them and at least for
	// AAHScout_MPR_sag, the frame count for some reason doesn't match NIFTI dimensions
	final protected String  LOCALIZER_RGX = "^Localizer.*";
	final protected String SCOUT_RGX = "(?i)^.*AA.*Scout.*";
	final protected Map<String, Integer[]> _scanTypeNiftiFileCountMap = new HashMap<>();
	final protected Map<String, Integer[]> _seriesDescNiftiFileCountMap = new HashMap<>();
	protected boolean seriesDescFileCountMapInitialized = false;
	final protected List<String> _seriesDescFrameCountExclusionRegexes = new ArrayList<String>(Arrays.asList(new String[] { SCOUT_RGX }));
	final protected List<String> _scanTypeFrameCountExclusionRegexes = new ArrayList<String>(Arrays.asList(new String[] { SCOUT_RGX }));
	final protected List<String> _seriesDescNiftiCheckExclusionRegexes = new ArrayList<String>();
	final protected List<String> _scanTypeNiftiCheckExclusionRegexes = new ArrayList<String>();
	
	public abstract void initializeScanTypeNiftiFileCountMap();
	
	public void initializeSeriesDescNiftiFileCountMap() {
		// We usually won't have a series description file count map (This is used mainly for initial frame checks where only
		// certain task scans skip frames.
		seriesDescFileCountMapInitialized = true;
	}
	
	@Override
	// NOTE:  This method is a modification of the validate method in the Dcm2NiiValidator class 
	public List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) throws SanityChecksException {
		
		if (_scanTypeNiftiFileCountMap.isEmpty()) {
			initializeScanTypeNiftiFileCountMap();
		}
		
		if (!seriesDescFileCountMapInitialized && _seriesDescNiftiFileCountMap.isEmpty()) {
			initializeSeriesDescNiftiFileCountMap();
		}
		
		initializeInitialFramesMaps(projectId);

		final List<SanityCheck> checkList = new ArrayList<>();
		for (final XnatImagescandataI scan : imageSession.getScans_scan()) {
			final StringBuilder infoSB = new StringBuilder();
			final String scanType = scan.getType();
			final String lCaseScanType = (scanType!=null) ? scanType.toLowerCase() : "";
			if (lCaseScanType.contains("physio") || lCaseScanType.contains("phoenixzip") || 
					lCaseScanType.contains("collection") || lCaseScanType.contains("posdisp:") || 
					(lCaseScanType.contains("&lt;") && lCaseScanType.contains("&gt;")) ||
					(lCaseScanType.contains("<") && lCaseScanType.contains(">"))) {
				continue;
			}
			final String scanQuality = scan.getQuality();
			final String seriesDesc = scan.getSeriesDescription();
			final String scanId = scan.getId();
			boolean hasSecondary = false;
			boolean hasNifti = false;
			boolean niftiFilesOk = true;
			boolean hasInitialFramesFile = false;
			boolean hasValidBidsJson = false;
			boolean hasDicom = false;
			boolean skipSecondaryScan= false;
			List<Integer> niftiDimensions = new ArrayList<>();
			XnatAbstractresourceI niftiResource = null;
			for (final XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				if (resource.getLabel().equals(CommonConstants.DICOM_RESOURCE)) {
					hasDicom = true;	
				} else if (resource.getLabel().equals(CommonConstants.NIFTI_RESOURCE)) {
					if (!(isScanTypeExcludedFromNiftiChecks(scanType) || isSeriesDescExcludedFromNiftiChecks(seriesDesc))) {
						hasNifti = true;
						niftiResource = resource;
						niftiFilesOk = ResourceCheckUtils.checkNiftiFiles((XnatResourcecatalog)resource, infoSB, scanId, seriesDesc);
						hasInitialFramesFile = ResourceCheckUtils.hasInitialFramesFile((XnatResourcecatalog)resource);
						try {
							niftiDimensions.addAll(CcfNiftiUtils.getNiftiDimensionSums((XnatResourcecatalog)resource));
						} catch (IOException e) {
							log.error("IO Exception thrown retrieving NIFTI Frame Counts",e);
						}
						hasValidBidsJson = ResourceCheckUtils.hasValidBidsJsonFile((XnatResourcecatalog)resource);
					}
				} else if (resource.getLabel().equalsIgnoreCase(CommonConstants.SECONDARY_RESOURCE)) {
					hasSecondary = true;
					if(!hasNIFTIResource(scan))
					{
						skipSecondaryScan=true;
						break;
					}
				}
			}
			if(skipSecondaryScan) {
				continue;
			}
			
			final SanityCheck sCheck = new SanityCheck(scan);
			sCheck.setOutcome(Outcome.PASSED);
			sCheck.setCheckLongName(NIFTI_CHECK_LONG_NAME);
			sCheck.setCheckShortName(NIFTI_CHECK_SHORT_NAME);
			sCheck.setOutcome(Outcome.PASSED);
			if (scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
				sCheck.setOutcome(Outcome.SCAN_UNUSABLE);
			} else if ((!hasNifti && hasDicom) || (!hasNifti && !hasSecondary)) {
				if (!(isScanTypeExcludedFromNiftiChecks(scanType) || isSeriesDescExcludedFromNiftiChecks(seriesDesc))) {
					infoSB.append("Missing NIFTI resource.  ");
					sCheck.setOutcome(Outcome.FAILED);
					sCheck.setDescriptionOfFailure(infoSB.toString());
				}
			} else if (!niftiFilesOk) {
				sCheck.setOutcome(Outcome.FAILED);
				sCheck.setDescriptionOfFailure(infoSB.toString());
			} 
			checkList.add(sCheck);
			
			if (niftiResource == null) {
				// No point in doing any further NIFTI checks if there's no resource.
				continue;
			}
			
			if (_initialFramesTypeMap.get(projectId).contains(scanType) || _initialFramesDescMap.get(projectId).contains(seriesDesc)) {
				
				final SanityCheck ifCheck = new SanityCheck(scan);
				ifCheck.setOutcome(Outcome.PASSED);
				ifCheck.setCheckLongName(INITIALFRAMES_NIFTI_CHECK_LONG_NAME);
				ifCheck.setCheckShortName(INITIALFRAMES_NIFTI_CHECK_SHORT_NAME);
				ifCheck.setOutcome(Outcome.PASSED);
				if (scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
					ifCheck.setOutcome(Outcome.SCAN_UNUSABLE);
				} else if (!hasInitialFramesFile) {
					ifCheck.setOutcome(Outcome.FAILED);
					ifCheck.setDescriptionOfFailure("Missing InitialFrames file.");
				}	
				checkList.add(ifCheck);
				
			}
			
			if (!(isSeriesDescExcludedFromFrameCountCheck(seriesDesc) || isScanTypeExcludedFromFrameCountCheck(scanType))) { 
				
				final SanityCheck fcCheck = new SanityCheck(scan);
				fcCheck.setOutcome(Outcome.PASSED);
				fcCheck.setCheckLongName(NIFTI_FRAMECOUNT_CHECK_LONG_NAME);
				fcCheck.setCheckShortName(NIFTI_FRAMECOUNT_CHECK_SHORT_NAME);
				fcCheck.setOutcome(Outcome.PASSED);
				if (scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
					fcCheck.setOutcome(Outcome.SCAN_UNUSABLE);
				} else if (!niftiDimensions.contains(scan.getFrames())) {
					final String failureDesc = "None of the NIFTI file dimensions (dim1-dim4, summed across all NIFTI files) match the frame "
							+ "count of scan (SCAN_FRAMES=" + scan.getFrames() + ", NIFTI_DIMENSIONS=" + niftiDimensions + ")";
					fcCheck.setOutcome(Outcome.FAILED);
					fcCheck.setDescriptionOfFailure(failureDesc);
				} 
				checkList.add(fcCheck);
				
			}

			// Very rarely, we exclude a series description from NIFTI checks
			if (!(isScanTypeExcludedFromNiftiChecks(scanType) || isSeriesDescExcludedFromNiftiChecks(seriesDesc))) {
			
				final SanityCheck fcCheck = new SanityCheck(scan);
				fcCheck.setOutcome(Outcome.PASSED);
				fcCheck.setCheckLongName(NIFTI_FILE_CHECKS_LONG_NAME);
				fcCheck.setCheckShortName(NIFTI_FILE_CHECKS_SHORT_NAME);
				if (scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
					fcCheck.setOutcome(Outcome.SCAN_UNUSABLE);
				} else {
					final CheckResult result = doNiftiFileChecks(scan, niftiResource);
					fcCheck.setOutcome(result.getOutcome());
					fcCheck.setDescriptionOfFailure(result.getDescriptionOfFailure());
				} 
				checkList.add(fcCheck);
				
				final SanityCheck jsonCheck = new SanityCheck(scan);
				jsonCheck.setOutcome(Outcome.PASSED);
				jsonCheck.setCheckLongName(BIDSJSON_CHECK_LONG_NAME);
				jsonCheck.setCheckShortName(BIDSJSON_CHECK_SHORT_NAME);
				if (scanQuality!=null && scanQuality.equalsIgnoreCase("UNUSABLE")) {
					jsonCheck.setOutcome(Outcome.SCAN_UNUSABLE);
				} else {
					final CheckResult result = new CheckResult((hasValidBidsJson) ? Outcome.PASSED : Outcome.FAILED);
					if (!hasValidBidsJson) {
						jsonCheck.setOutcome(result.getOutcome());
						jsonCheck.setDescriptionOfFailure("Has missing, empty or invalid BIDS JSON sidecar file.");
					}
				} 
				checkList.add(jsonCheck);

			}
			
		}
		doNonNiftiChecks(projectId, imageSession, user, checkList);
		return checkList;
	}

	protected boolean isScanTypeExcludedFromFrameCountCheck(String scanType) {
		return isExcluded(scanType, _scanTypeFrameCountExclusionRegexes);
	}

	protected boolean isSeriesDescExcludedFromFrameCountCheck(String seriesDesc) {
		return isExcluded(seriesDesc, _seriesDescFrameCountExclusionRegexes);
	}

	protected boolean isScanTypeExcludedFromNiftiChecks(String scanType) {
		return isExcluded(scanType, _scanTypeNiftiCheckExclusionRegexes);
	}

	protected boolean isSeriesDescExcludedFromNiftiChecks(String seriesDesc) {
		return isExcluded(seriesDesc, _seriesDescNiftiCheckExclusionRegexes);
	}

	protected boolean isExcluded(String ins, List<String> regexes) {
		if (ins == null || ins.length()<1) {
			return true;
		}
		for (final String regex : regexes) {
			if (ins.matches(regex)) {
				return true;
			}
		}
		return false;
	}

	protected CheckResult doNiftiFileChecks(XnatImagescandataI scan, XnatAbstractresourceI niftiResource) {
		
		final XnatResourcecatalog dicomResource = ResourceUtils.getResource(scan, CommonConstants.DICOM_RESOURCE);
		final StringBuilder sb = new StringBuilder();
		final String  scanType = scan.getType();
		final String  seriesDesc = scan.getSeriesDescription();

		final CheckResult result = new CheckResult(Outcome.PASSED);
		// Very rarely, we exclude a series description from NIFTI checks
		if (isScanTypeExcludedFromNiftiChecks(scanType) || isSeriesDescExcludedFromNiftiChecks(seriesDesc)) {
			return result;
		}
		if (scanType == null || seriesDesc == null) {
			// Prevent NPE:  No checks
			return result;
		}
		// Check file count
		if (_scanTypeNiftiFileCountMap.containsKey(scanType)) {
			final Integer fileCount = niftiResource.getFileCount();
			final Integer[] expectedCountArr = _scanTypeNiftiFileCountMap.get(scanType);
			final List<Integer> expectedCount = Arrays.asList(expectedCountArr);
			if (!expectedCount.contains(fileCount)) {
				result.setOutcome(Outcome.FAILED);
				sb.append(String.format("Incorrect NIFTI file Count:  has %s,  should be %s.  ",
						fileCount, Arrays.toString(expectedCountArr)));
			}
		}
		if (_seriesDescNiftiFileCountMap.containsKey(seriesDesc)) {
			final Integer fileCount = niftiResource.getFileCount();
			final Integer[] expectedCountArr = _seriesDescNiftiFileCountMap.get(seriesDesc);
			final List<Integer> expectedCount = Arrays.asList(expectedCountArr);
			if (!expectedCount.contains(fileCount)) {
				result.setOutcome(Outcome.FAILED);
				sb.append(String.format("Incorrect NIFTI file Count:  has %s,  should be %s.  ",
						fileCount, Arrays.toString(expectedCountArr)));
			}
		}
		if (niftiResource instanceof XnatResourcecatalog) {
			// Check file name
			for (final Object fNameObj : ((XnatResourcecatalog)niftiResource).getCorrespondingFileNames(CommonConstants.ROOT_PATH)) {
				final String fName = fNameObj.toString(); 
				if (fName.toLowerCase().contains(".nii")) {
					if (!seriesDescModifier(fName).contains(seriesDescModifier(seriesDesc))) {
						result.setOutcome(Outcome.FAILED);
						sb.append(String.format("Incorrect NIFTI file name (%s).  It should contain the series description.  ",fName));
					} else {
						// Make sure file is big enough to have images
						File niftiFile = ResourceUtils.getFileByFileName(niftiResource, fName);
						if (niftiFile.exists() && niftiFile.length()<10000) {
							result.setOutcome(Outcome.FAILED);
							sb.append("NIFTI file is too small to contain images (BYTES=" +   niftiFile.length() + ")");
						}
					}
				}
			}
			// Check NIFTI date
			Long niftiDate = ResourceUtils.getEarliestFileModTime((XnatResourcecatalog)niftiResource, 20);
			Long dicomDate = ResourceUtils.getRecentFileModTime(dicomResource, 20);
			if (dicomDate != null && niftiDate != null && dicomDate>niftiDate) {
				result.setOutcome(Outcome.FAILED);
				sb.append("NIFTI resource is older than DICOM  ");
			}
		}
		result.setDescriptionOfFailure(sb.toString());
		return result;
	}

	protected String seriesDescModifier(String seriesDesc) {
		return seriesDesc;
	}

	protected void initializeInitialFramesMaps(final String projectId) {
		if (!_initialFramesTypeMap.containsKey(projectId)) {
			_initialFramesTypeMap.put(projectId, new ArrayList<String>());
		}
		if (!_initialFramesDescMap.containsKey(projectId)) {
			_initialFramesDescMap.put(projectId, new ArrayList<String>());
		}
	}

	public abstract void doNonNiftiChecks(String projectId, XnatImagesessiondataI imageSession, 
			UserI user, List<SanityCheck> checkList) throws SanityChecksException;

	private boolean hasNIFTIResource(XnatImagescandataI scan) {
		return ResourceUtils.hasResource(scan, CommonConstants.NIFTI_RESOURCE);
	}
	
}
