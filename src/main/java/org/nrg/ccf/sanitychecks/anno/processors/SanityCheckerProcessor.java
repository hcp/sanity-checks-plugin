package org.nrg.ccf.sanitychecks.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.sanitychecks.anno.SanityChecker")
public class SanityCheckerProcessor extends NrgAbstractAnnotationProcessor<SanityChecker> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, SanityChecker annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(SanityChecker.SANITY_CHECKER, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, SanityChecker annotation) {
        return String.format("sanitychecker/%s-sanitychecker.properties", element.getSimpleName());
	}

}
