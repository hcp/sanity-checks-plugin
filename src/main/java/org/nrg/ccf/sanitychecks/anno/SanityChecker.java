package org.nrg.ccf.sanitychecks.anno;

public @interface SanityChecker {
	
	String SANITY_CHECKER = "sanityChecker";

	String description() default "";

}
