package org.nrg.ccf.sanitychecks.dto;

import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;

public class SanityCheck {
	
	private String sessionId;
	private String scanNumber;
	private String seriesDesc;
	private String checkShortName;
	private String checkLongName;
	private String acceptJustification = "";
	private String acceptUsername = "";
	private Outcome outcome = null;
	private String descriptionOfFailure = "";
	
	public SanityCheck() { }
	
	public SanityCheck(XnatImagesessiondataI imageSession) {
		this.sessionId = imageSession.getId();
		this.scanNumber = "";
		this.seriesDesc = "";
	}
	public SanityCheck(XnatImagescandataI scan) {
		this.sessionId = scan.getImageSessionId();
		this.scanNumber = scan.getId();
		this.seriesDesc = scan.getSeriesDescription();
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getScanNumber() {
		return scanNumber;
	}
	public void setScanNumber(String scanNumber) {
		this.scanNumber = scanNumber;
	}
	public String getSeriesDesc() {
		return seriesDesc;
	}
	public void setSeriesDesc(String seriesDesc) {
		this.seriesDesc = seriesDesc;
	}
	public String getCheckShortName() {
		return checkShortName;
	}
	public void setCheckShortName(String checkShortName) {
		this.checkShortName = checkShortName;
	}
	public String getCheckLongName() {
		return checkLongName;
	}
	public void setCheckLongName(String checkLongName) {
		this.checkLongName = checkLongName;
	}
	public Outcome getOutcome() {
		return outcome;
	}
	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}
	public String getDescriptionOfFailure() {
		return descriptionOfFailure;
	}
	public void setDescriptionOfFailure(String descriptionOfFailure) {
		this.descriptionOfFailure = descriptionOfFailure;
	}
	public String getAcceptJustification() {
		return acceptJustification;
	}
	public void setAcceptJustification(String acceptJustification) {
		this.acceptJustification = acceptJustification;
	}
	public String getAcceptUsername() {
		return acceptUsername;
	}
	public void setAcceptUsername(String acceptUsername) {
		this.acceptUsername = acceptUsername;
	}
	
}
