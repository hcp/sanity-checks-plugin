package org.nrg.ccf.sanitychecks.dto;

public enum Outcome {
	
	PASSED, FAILED, WARNING, ACCEPTED, SCAN_UNUSABLE
	
}
