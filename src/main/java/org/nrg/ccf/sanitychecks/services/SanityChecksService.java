package org.nrg.ccf.sanitychecks.services;

import java.io.InvalidClassException;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.automation.entities.ScriptOutput;
import org.nrg.automation.services.ScriptRunnerService;
import org.nrg.ccf.sanitychecks.components.SanityChecksResults;
import org.nrg.ccf.sanitychecks.components.SanityChecksSettings;
import org.nrg.ccf.sanitychecks.components.SanityChecksSettingsInfo;
import org.nrg.ccf.sanitychecks.constants.CheckerType;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.exception.SanityChecksScriptRunException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.ccf.sanitychecks.utils.SanityCheckUtils;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SanityChecksService {
	
	public static final String CONFIG_TOOL_NAME = "sanity_checks";
	public static final String CONFIG_PATH_TO_FILE = "configuration";
	private final ScriptRunnerService _runnerService;
	private final SanityChecksSettings _sanityChecksSettings;
	final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(SanityChecksService.class);
	
	/*
	private DataSource _dataSource;
	
	@PostConstruct
	public void init() {
		// Move data from old sanity checks tables if necessary
		_logger.info("SanityChecksInit - Checking whether or not data needs to be converted from old schema tables to new tables.");
		try {
			final JdbcTemplate template = new JdbcTemplate(_dataSource);
	        final List<String> oldTableQuery = template.query(
	        		"SELECT DISTINCT table_name FROM information_schema.tables WHERE table_name like '%val%sanity%checks%'"
	                , new RowMapper<String>() {
	                    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
	                        return rs.getString(1);
	                    }
	                });
	        if (oldTableQuery.isEmpty()) {
	        	return;
	        }
	        final List<String> newDataQuery = template.query(
	        		"SELECT DISTINCT id FROM san_sanitychecks"
	                , new RowMapper<String>() {
	                    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
	                        return rs.getString(1);
	                    }
	                });
	        final List<String> oldDataQuery = template.query(
	        		"SELECT DISTINCT id FROM val_sanitychecks"
	                , new RowMapper<String>() {
	                    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
	                        return rs.getString(1);
	                    }
	                });
	        if (newDataQuery.isEmpty() && !oldDataQuery.isEmpty()) {
	        	_logger.info("SanityChecksInit - Data needs to be converted - Run conversion sql.");
	        	try {
	        		template.execute(
		        		"INSERT INTO san_sanitychecks_meta_data SELECT * from val_sanitychecks_meta_data; " +
		        		"INSERT INTO san_sanitychecks SELECT * from val_sanitychecks; " +
		        		"INSERT INTO san_sanitychecks_check_meta_data SELECT * from val_sanitychecks_check_meta_data; " +
		        		"INSERT INTO san_sanitychecks_check(description,diagnosis,id,status,sanitychecks_check_info, " +
		        		"		san_sanitychecks_check_id,session_session_checks_check_sa_id) " +
		        		"		 SELECT description,diagnosis,id,status,sanitychecks_check_info,  " +
		        		"		val_sanitychecks_check_id,session_session_checks_check_va_id from val_sanitychecks_check; " +
		        		"INSERT INTO san_sanitychecks_scan_checks_meta_data SELECT * from val_sanitychecks_scan_checks_meta_data; " +        					
		        		"INSERT INTO san_sanitychecks_scan_checks(statusoverride_username,statusoverride_datetime,statusoverride_justification, " +
		        		"		scan_id,status,sanitychecks_scan_checks_info,san_sanitychecks_scan_checks_id,scans_scan_checks_san_sanityche_id) " + 
		        		"		 SELECT statusoverride_username,statusoverride_datetime,statusoverride_justification, " +
		        		"		scan_id,status,sanitychecks_scan_checks_info,val_sanitychecks_scan_checks_id,scans_scan_checks_val_sanityche_id " + 
		        		"		 from val_sanitychecks_scan_checks; " +
		         		"INSERT INTO san_sanitychecks_scan_checks_check_meta_data SELECT * from val_sanitychecks_scan_checks_check_meta_data; " +       					 
		        		"INSERT INTO san_sanitychecks_scan_checks_check(description,diagnosis,id,status,sanitychecks_scan_checks_check_info,  " +
		        		"		san_sanitychecks_scan_checks_check_id,san_sanitychecks_scan_checks_san_sanitychecks_scan_checks_id)  " +
		        		"		 SELECT description,diagnosis,id,status,sanitychecks_scan_checks_check_info,  " +
		        		"		val_sanitychecks_scan_checks_check_id,val_sanitychecks_scan_checks_val_sanitychecks_scan_checks_id  " +
	        			"		 from val_sanitychecks_scan_checks_check; ");
	        		_logger.info("SanityChecksInit - Data conversion complete.");
	        	} catch (Throwable t) {
	        		_logger.info("SanityChecksInit - Exception thrown during conversion.",t);
	        		throw t;
	        	}
	        } else {
	        	_logger.info("SanityChecksInit - No data conversion necessary.");
	        }
			_logger.info("SanityChecksInit - Checking whether or not experiment extension needs to be modified to point to new data type.");
	        Integer oldExtension = template.queryForObject("SELECT xdat_meta_element_id FROM xdat_meta_element WHERE element_name='val:sanityChecks'", Integer.class);
	        Integer newExtension = template.queryForObject("SELECT xdat_meta_element_id FROM xdat_meta_element WHERE element_name='san:sanityChecks'", Integer.class);
	        if (oldExtension!=null && newExtension!=null) {
	        	List<String> oldTypeList = template.queryForList("SELECT id FROM xnat_experimentdata where extension=" + oldExtension + ";", String.class);
	        	if (!oldTypeList.isEmpty()) {
	        		_logger.info("SanityChecksInit - Experiment conversion necessary.  Starting conversion.");
	        		try {
	        			template.execute("UPDATE xnat_experimentdata SET extension=" + newExtension + " WHERE extension=" + oldExtension);
	        			_logger.info("SanityChecksInit - Experiment conversion complete.");
	        		} catch (Throwable t) {
	        			_logger.info("SanityChecksInit - Exception thrown during conversion.",t);
	        			throw t;
	        		}
	        	} else {
	        		_logger.info("SanityChecksInit - No experiment conversion necessary.");
	        	}
	        	
	        } else {
	        	_logger.info("SanityChecksInit - Either the old data type or the new one has not been set up.  Skipping conversion.");
	        	
	        }
        } catch (Exception e) {
        	_logger.info("SanityChecksInit - Exception thrown during update process.  Skipping conversion.",e);
        }
	}

	@Autowired
	public SanityChecksService(ScriptRunnerService runnerService, DataSource dataSource, SanityChecksSettings sanityChecksSettings) {
		super();
		_runnerService = runnerService;
		_dataSource = dataSource;
		_sanityChecksSettings = sanityChecksSettings;
	}
	*/
	
	@Autowired
	public SanityChecksService(ScriptRunnerService runnerService, SanityChecksSettings sanityChecksSettings) {
		super();
		_runnerService = runnerService;
		_sanityChecksSettings = sanityChecksSettings;
	}
	
	public SanityChecksResults runSanityChecks(UserI user, String projectId, XnatMrsessiondata exp) 
			throws SanityChecksScriptRunException, SanityChecksException {
		final SanityChecksSettingsInfo settings = _sanityChecksSettings.getInfo(projectId);
		final SanityChecksResults results = new SanityChecksResults();
		if (settings.getCheckerType().equals(CheckerType.JAVA_CLASS)) {
			final String checkClass = settings.getChecksClass();
			SanityCheckerI checker;
			try {
				checker = SanityCheckUtils.getSanityChecker(checkClass);
			} catch (BeansException | InvalidClassException | ClassNotFoundException e) {
				log.error(e.toString());
				throw new SanityChecksScriptRunException("Couldn't instantiate SanityCheckerI", e);
			}
			try {
				results.setHttpStatus(SanityCheckUtils.runChecks(projectId, exp.getSubjectId(), exp.getId(), checker, user));
			} catch (Exception e) {
				log.error("ERROR:  Unexpected exception thrown running sanity checks", e);
				results.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
				results.setRunInfo("ERROR:  Unexpected exception thrown running sanity checks:  " + ExceptionUtils.getFullStackTrace(e));
			}
		} else {
			final String scriptId = settings.getScriptId();
			final ScriptOutput output = runScript(user, projectId, exp, scriptId);
			results.setScriptOutput(output);
		}
		return results;
	}
	
	private ScriptOutput runScript(UserI user, String projectId, XnatMrsessiondata exp, final String scriptID) throws SanityChecksScriptRunException {
		if (scriptID == null || scriptID.length()<1) {
			throw new SanityChecksScriptRunException("ERROR:  No script has been defined in this projects's sanity checks configuration.");
		}
        final Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("user", user);
        parameters.put("projectId", projectId);
        parameters.put("subjectLbl", exp.getSubjectData().getLabel());
        parameters.put("experimentLbl", exp.getLabel());
        try {
        	return _runnerService.runScript(_runnerService.getScript(scriptID), null, parameters, false);
        } catch (NrgServiceException e) {
            final String message = String.format("Failed running sanity checks script for experiment %s", exp.getLabel());
            AdminUtils.sendAdminEmail("Script execution failure", message);
            _logger.error(message, e);
            throw new SanityChecksScriptRunException("Sanity checks script not successfully completed.",e);
        }
	}

}
