package org.nrg.ccf.sanitychecks.constants;

import org.nrg.ccf.sanitychecks.dto.Outcome;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckResult {
	
	Outcome outcome;
	String descriptionOfFailure;
	
	public CheckResult() {
		super();
		this.descriptionOfFailure = "";
	}
	
	public CheckResult(Outcome outcome) {
		super();
		this.outcome = outcome;
		this.descriptionOfFailure = "";
	}
	
	public CheckResult(Outcome outcome, String descriptionOfFailure) {
		super();
		this.outcome = outcome;
		this.descriptionOfFailure = descriptionOfFailure;
	}
	
}
