package org.nrg.ccf.sanitychecks.constants;

public enum CheckerType {
	
	JAVA_CLASS, AUTOMATION_SCRIPT

}
