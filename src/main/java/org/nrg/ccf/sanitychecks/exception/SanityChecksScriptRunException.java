package org.nrg.ccf.sanitychecks.exception;

public class SanityChecksScriptRunException extends Exception {

	/**
	 * Instantiates a new sanity checks script run exception.
	 *
	 * @param string the string
	 */
	public SanityChecksScriptRunException(String string) {
		super(string);
	}
	
	public SanityChecksScriptRunException(String string, Throwable t) {
		super(string, t);
	}

	private static final long serialVersionUID = 743171733964979985L;

}
