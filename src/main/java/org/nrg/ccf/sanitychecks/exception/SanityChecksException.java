package org.nrg.ccf.sanitychecks.exception;

public class SanityChecksException extends Exception {

	private static final long serialVersionUID = -1989580873504250259L;
	
	public SanityChecksException(String msg,Throwable e){
		super(msg,e);
	}
	
	public SanityChecksException(String msg){
		super(msg);
	}

}
