package org.nrg.ccf.sanitychecks.queries;

public class SanityChecksQueries {
	
	final static public String WHERE_REPLACE = "$$$WHERE$$$"; 
	
	final static public String SESSION_INFO_QUERY = 
			"SELECT se.project as project, se.id as id, se.label as label, ise.scanner as scanner, ise.operator as operator" +
			"      FROM" +
			"         xnat_experimentdata se" +
			"         INNER JOIN" +
			"         xnat_imagesessiondata ise" +
			"         ON se.id=ise.id" +
			"         INNER JOIN" +
			"         xnat_mrsessiondata mrse" +
			"         ON se.id=mrse.id" +
			"	" + WHERE_REPLACE + " " +
			"     ;" 
	;
	
	final static public String SCAN_INFO_QUERY = 
			"SELECT se.id AS imageSessionId, " +
			"   isc.id AS id, isc.type AS type,isc.series_description AS seriesDescription," +
			"	isc.quality AS quality" +
			"      FROM" +
			"         xnat_experimentdata se" +
			"         INNER JOIN" +
			"         xnat_imagesessiondata ise" +
			"         ON se.id=ise.id" +
			"         INNER JOIN" +
			"         xnat_mrsessiondata mrse" +
			"         ON se.id=mrse.id" +
			"         INNER JOIN" +
			"         xnat_imagescandata isc" +
			"         ON se.id=isc.image_session_id" +
			"         INNER JOIN" +
			"         xnat_mrscandata mrsc" +
			"         ON isc.xnat_imagescandata_id=mrsc.xnat_imagescandata_id" +
			"	" + WHERE_REPLACE + " " +
			"           and type != 'AAHScout' and type not like 'Localizer%' " +
			"           and type not like '%_SBRef' and type not like 'FieldMap%'" +
			"     ;" 
	;
	
	final static public String FAILED_SESSION_CHECK_INFO_QUERY = 
			"SELECT  exp.id as id, exp.project as project, exp.label as label, imexp.id as imageSessionId, imexp.label as imageSessionLabel, " +
			"        imexp.date as imageSessionDate, imsess.scanner as scanner, imsess.operator as operator, " +
			"        san.overall_status as overallStatus, san.session_status as sessionStatus, " +
			"        ck.status as status, ck.description as description, ck.diagnosis as diagnosis, ck.id as checkId " +
			" FROM " +
			"     xnat_experimentdata exp " +
			"     INNER JOIN " +
			"     xnat_imageassessordata ia " +
			"     ON exp.id = ia.id " +
			"     LEFT JOIN " +
			"     xnat_experimentdata imexp " +
			"     ON imexp.id = ia.imagesession_id " +
			"     INNER JOIN " +
			"     xnat_imagesessiondata imsess " +
			"     ON imexp.id = imsess.id " +
			"     INNER JOIN " +
			"     san_sanitychecks san " +
			"     ON exp.id = san.id " +
			"     INNER JOIN " +
			"     san_sanitychecks_check ck " +
			"     ON san.id = ck.session_session_checks_check_sa_id " +
			"	" + WHERE_REPLACE + " " +
			" AND ck.status = 'FAILED' " +
			"ORDER BY label " +
			"; " 
	;
	
	final static public String FAILED_SCAN_CHECK_INFO_QUERY = 
			"SELECT  exp.id as id, exp.project as project, exp.label as label, imexp.id as imageSessionId, imexp.label as imageSessionLabel, " +
			"        imexp.date as imageSessionDate, imsess.scanner as scanner, imsess.operator as operator, " +
			"        san.overall_status as overallStatus,  " +
			"        CASE WHEN ck.scan_id ~ '^[0-9]+$' THEN to_number(ck.scan_id,'99999') ELSE NULL END AS numScanId, " +
			"        ck.scan_id as scanId, isc.series_description as seriesDescription, isc.type as scanType, isc.quality as scanQuality,   " +
			"        ck.status as scanStatus, sck.status as status, sck.description as description, sck.diagnosis as diagnosis, sck.id as checkId " +
			" FROM " +
			"     xnat_experimentdata exp " +
			"     INNER JOIN " +
			"     san_sanitychecks san " +
			"     ON exp.id = san.id " +
			"     INNER JOIN " +
			"     xnat_imageassessordata ia " +
			"     ON exp.id = ia.id " +
			"     LEFT JOIN " +
			"     xnat_experimentdata imexp " +
			"     ON imexp.id = ia.imagesession_id " +
			"     INNER JOIN " +
			"     xnat_imagesessiondata imsess " +
			"     ON imexp.id = imsess.id   " +
			"     INNER JOIN " +
			"     san_sanitychecks_scan_checks ck " +
			"     ON san.id = ck.scans_scan_checks_san_sanityche_id " +
			"     LEFT JOIN " +
			"     san_sanitychecks_scan_checks_check sck " +
			"     ON ck.san_sanitychecks_scan_checks_id = sck.san_sanitychecks_scan_checks_san_sanitychecks_scan_checks_id  " +
			"     INNER JOIN " +
			"     xnat_imagescandata isc " +
			"     ON ia.imagesession_id = isc.image_session_id AND isc.id=ck.scan_id " +
			"	" + WHERE_REPLACE + " " +
			" AND ck.status != 'SCAN_UNUSABLE' AND sck.status = 'FAILED' " +
			"ORDER BY label,numScanId,scanId " +
			";  " 
	;
	
}
