package org.nrg.ccf.sanitychecks.inter;

import java.util.List;

import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;

public interface SanityCheckerI extends BaseSanityCheckerI {
	
	List<SanityCheck> doChecks(String projectId, XnatImagesessiondataI imageSession, UserI user) throws SanityChecksException;

}
