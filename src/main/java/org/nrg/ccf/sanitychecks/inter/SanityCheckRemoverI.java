package org.nrg.ccf.sanitychecks.inter;

import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xft.security.UserI;

public interface SanityCheckRemoverI {
	
	boolean hasChecksToRemove();

	void removeChecks(SanSanitychecks sanityChecks, UserI user) throws SanityChecksException;

}
