package org.nrg.ccf.sanitychecks.xapi;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.sanitychecks.utils.SanityChecksReportUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Sanity Checks Report API")
public class SanityChecksReportApi extends AbstractXapiRestController {

	//private static final Logger _logger = LoggerFactory.getLogger(SanityChecksReportApi.class);

	private JdbcTemplate _jdbcTemplate;

	@Autowired
	protected SanityChecksReportApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, JdbcTemplate jdbcTemplate) {
		super(userManagementService, roleHolder);
		_jdbcTemplate = jdbcTemplate;
	}
	
	
	@ApiOperation(value = "Gets Sanity Checks Failure Report Table", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = { "sanityChecksReports/project/{projectId}/failureReport" },
    						restrictTo=AccessLevel.Read,
    							produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> getScFailureReport(
    			@PathVariable("projectId") final String projectId) throws NrgServiceException {
		final List<Map<String,String>> records = SanityChecksReportUtils.getFailureReportTable(projectId, getSessionUser(), _jdbcTemplate);
		return new ResponseEntity<>(records,HttpStatus.OK);
	}
	
	@ApiOperation(value = "Gets Sanity Checks Failure Report CSV", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = { "sanityChecksReports/project/{projectId}/failureReportCSV" },
    						restrictTo=AccessLevel.Read,
    							produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getScFailureReportCSV(
    			@PathVariable("projectId") final String projectId) throws NrgServiceException {
		return new ResponseEntity<>(SanityChecksReportUtils.getFailureReportCSV(projectId,getSessionUser(), _jdbcTemplate),HttpStatus.OK);
	}
	
}

