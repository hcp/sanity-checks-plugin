package org.nrg.ccf.sanitychecks.xapi;

import java.io.InvalidClassException;

import org.nrg.ccf.sanitychecks.components.SanityChecksSettings;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.ccf.sanitychecks.utils.SanityCheckUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@XapiRestController
@Api(description = "Sanity Checks API")
@Slf4j
public class SanityChecksApi extends AbstractXapiRestController {
	
	final SanityChecksSettings _settings;

	@Autowired
	protected SanityChecksApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, SanityChecksSettings settings) { 
		super(userManagementService, roleHolder);
		_settings = settings;
	}
	
	
	@ApiOperation(value = "Runs Java-based sanity checks", 
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {
    			"/projects/{projectId}/subjects/{subjectId}/experiments/{experimentId}/runJavaBasedChecks",
    			"/projects/{projectId}/experiments/{experimentId}/runJavaBasedChecks"},
    						restrictTo=AccessLevel.Member,
    							produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> runSanityChecks(
    			@PathVariable("projectId") final String projectId,
    			@PathVariable(value="subjectId", required=false) String subjectId,
    			@PathVariable("experimentId") String experimentId,
    			@RequestParam(value="checkClass", required=false) String checkClass) throws NrgServiceException {
		final SanityCheckerI checker;
		try {
			if (checkClass == null) {
				// Use configured checkClass if not passed
				checkClass = _settings.getChecksClass(projectId);
			}
			checker = SanityCheckUtils.getSanityChecker(checkClass);
			if (checker == null) {
				log.error("ERROR:  Could not obtain checker class (CLASS=" + checkClass + ")");
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (BeansException | InvalidClassException | ClassNotFoundException e) {
			log.error(e.toString());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		if (subjectId == null || subjectId.toString().length() == 0) {
			// The experiment ID is likely for a sanity check rather than an experiment
			final SanSanitychecks sc = SanSanitychecks.getSanSanitycheckssById(experimentId, getSessionUser(), false);
			if (sc != null) {
				experimentId = sc.getImagesessionId();
				if (experimentId != null && experimentId.length()>0) {
					XnatImagesessiondata iSess = XnatImagesessiondata.getXnatImagesessiondatasById(experimentId, getSessionUser(), false);
					subjectId = iSess.getSubjectId();
				}
			}
			
		}
		HttpStatus status;
		try {
			status = SanityCheckUtils.runChecks(projectId, subjectId, experimentId, checker,getSessionUser());
			return new ResponseEntity<>(status);
		} catch (SanityChecksException e) {
			log.info(e.toString());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
}
