package org.nrg.ccf.sanitychecks.xapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.nrg.ccf.sanitychecks.anno.SanityChecker;
import org.nrg.ccf.sanitychecks.components.SanityChecksSettings;
import org.nrg.ccf.sanitychecks.components.SanityChecksSettingsInfo;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@XapiRestController
@Api(description = "Sanity Checks Settings API")
public class SanityChecksSettingsApi extends AbstractXapiRestController {

	private SanityChecksSettings _sanityChecksSettings;
	private List<Resource> _resourceList;
	final List<String> _checksClasses = new ArrayList<>();

	@Autowired
	protected SanityChecksSettingsApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, 
			SanityChecksSettings sanityChecksSettings) {
		super(userManagementService, roleHolder);
		_sanityChecksSettings = sanityChecksSettings;
		try {
			for (final Resource resource : getResourceList()) {
				final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
				if (!properties.containsKey(SanityChecker.SANITY_CHECKER)) {
					continue;
				}
				_checksClasses.add(properties.getProperty(SanityChecker.SANITY_CHECKER));
			}
		} catch (IOException e) {
			log.error("ERROR:  Exception encountered when loading properties:  ",e);
		}
	}
	
	@ApiOperation(value = "Gets project sanity checks settings", notes = "Returns project-level sanity checks settings.",
			response = SanityChecksSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/sanityChecksSettings/{projectId}/settings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<SanityChecksSettingsInfo> getSanityChecksSettings(@PathVariable("projectId") final String projectId) throws NrgServiceException {
		
		return new ResponseEntity<SanityChecksSettingsInfo>(_sanityChecksSettings.getInfo(projectId),HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Sets project sanity checks settings", notes = "Sets project-level sanity checks settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/sanityChecksSettings/{projectId}/settings"}, restrictTo=AccessLevel.Owner, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setSanityChecksSettings(@PathVariable("projectId") final String projectId, @RequestBody final SanityChecksSettingsInfo settings) throws NrgServiceException {
		
		_sanityChecksSettings.setEnabled(projectId, settings.getEnabled());
		_sanityChecksSettings.setCheckerType(projectId, settings.getCheckerType());
		_sanityChecksSettings.setChecksClass(projectId, settings.getChecksClass());
		_sanityChecksSettings.setScriptId(projectId, settings.getScriptId());
		return new ResponseEntity<>(HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Gets project sanity checks settings", notes = "Returns project-level sanity checks settings.",
			response = String.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/sanityChecksSettings/checksClasses"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getChecksClasses() throws NrgServiceException {
		
		return new ResponseEntity<List<String>>(_checksClasses,HttpStatus.OK);
		
    }
	
	private List<Resource> getResourceList() throws IOException {
		return (_resourceList!=null) ? _resourceList : BasicXnatResourceLocator.getResources("classpath*:META-INF/xnat/sanitychecker/*-sanitychecker.properties");
	}
	

}
