package org.nrg.ccf.sanitychecks.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.LifespanUtils;
import org.nrg.ccf.sanitychecks.components.CheckInfo;
import org.nrg.ccf.sanitychecks.components.FailedScanCheckInfo;
import org.nrg.ccf.sanitychecks.components.FailedSessionCheckInfo;
import org.nrg.ccf.sanitychecks.queries.SanityChecksQueries;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class SanityChecksReportUtils {
	
	static final String[] VALIDATION_CHECKS = new String[] { "HAS_VALIDATION", "SCAN_VALIDATION_PASSED", "VALIDATION_PASSED" };
	static final String[] LINKEDDATA_CHECKS = new String[] { "has_design", "has_sess_psydat_carit", "has_sess_psydat_emotion",
			"has_sess_psydat_facename", "has_sess_psydat_guessing", "has_sess_psydat_psydat", "has_sess_psydat_rest",
			"has_sess_psydat_vismotor", "has_wide", "linked_data", "has_facename", "PHYSIO_EV_CHECK" };
	static final String[] COMPLETION_CHECKS = new String[] { "COMPLETION_CHECK" };
	static final String[] PIPELINE_CHECKS = new String[] { "func-qc", "nifti", "facemask" };
	
    static final Comparator<Map<String, String>> COMPARATOR = new Comparator<Map<String, String>>() {

			@Override
			public int compare(final Map<String, String> o1, final Map<String, String> o2) {
				int i = o1.get("label").compareTo(o2.get("label"));
				if (i != 0) return i;
				i = o1.get("scanID").compareTo(o2.get("scanID"));
				if (i != 0) return i;
				i = o1.get("checkType").compareTo(o2.get("checkType"));
				if (i != 0) return i;
				i = o1.get("checkID").compareTo(o2.get("checkID"));
				return i;		
			}
      		
    };

	public static List<Map<String, String>> getFailureReportTable(final String projectId, final UserI sessionUser, final JdbcTemplate jdbcTemplate) {
		
		final List<FailedSessionCheckInfo> sessionCheckInfoList = getSessionCheckInfo(projectId, sessionUser, jdbcTemplate);
		final List<FailedScanCheckInfo> scanCheckInfoList = getScanCheckInfo(projectId, sessionUser, jdbcTemplate);
		final List<Map<String, String>> returnList = new ArrayList<>();
		for (final FailedSessionCheckInfo sessionCheck : sessionCheckInfoList) {
			// COMMENTED OUT:  This is done in the query
			//if (!sessionCheck.getStatus().equalsIgnoreCase("FAILED")) {
			//	continue;
			//}
			final Map<String,String> failureMap = newFailureMap(sessionCheck);
			failureMap.put("scanID", "");
			failureMap.put("seriesDesc", "");
			failureMap.put("checkID", sessionCheck.getCheckId());
			failureMap.put("checkDescription", sessionCheck.getDescription());
			failureMap.put("checkType", getCheckType(sessionCheck.getCheckId()));
			failureMap.put("checkDiagnosis", sessionCheck.getDiagnosis());
			returnList.add(failureMap);
		}
		for (final FailedScanCheckInfo scanCheck : scanCheckInfoList) {
			// COMMENTED OUT:  This is done in the query
			//if (scanCheck.getStatus().equalsIgnoreCase("FAILED")) {
			//	continue;
			//}
			final Map<String,String> failureMap = newFailureMap(scanCheck);
			failureMap.put("scanID", scanCheck.getScanId());
			failureMap.put("scanType", scanCheck.getScanType());
			failureMap.put("seriesDesc", scanCheck.getSeriesDescription());
			failureMap.put("checkID", scanCheck.getCheckId());
			failureMap.put("checkDescription", scanCheck.getDescription());
			failureMap.put("checkType", getCheckType(scanCheck.getCheckId()));
			failureMap.put("checkDiagnosis", scanCheck.getDiagnosis());
			returnList.add(failureMap);
		}
		Collections.sort(returnList, COMPARATOR);
		return returnList;
		
	}

	public static String getFailureReportCSV(final String projectId,final UserI sessionUser,final JdbcTemplate jdbcTemplate) {
		
		final List<Map<String,String>> reportList = getFailureReportTable(projectId, sessionUser, jdbcTemplate); 
		if (reportList.size()<1) {
			return "";
		}
        final SortedSet<String> keys = new TreeSet<>(reportList.get(0).keySet());
        final StringBuffer sb = new StringBuffer();
        sb.append(StringUtils.join(keys, ","));
        sb.append(System.lineSeparator());
        for (final Map<String,String> map : reportList) {
        	final Iterator<String> i = keys.iterator(); 
        	while (i.hasNext()) {
        		final String key = i.next();
        		String val = map.get(key);
        		if (val == null) { 
        			val = "";
        		}
        		val = (!val.contains(",")) ? val : "\"" + val.replaceAll("\"", "\\\\\"") + "\"";
        		sb.append(val);
        		if (i.hasNext()) {
        			sb.append(",");
        		}
        		
        	}
       		sb.append(System.lineSeparator());
        }
        return sb.toString();
        
	}

	private static String getCheckType(final String id) {
		if (Arrays.asList(LINKEDDATA_CHECKS).contains(id)) {
			return "LINKED_DATA";
		} else if (Arrays.asList(VALIDATION_CHECKS).contains(id)) {
			return "VALIDATION";
		} else if (Arrays.asList(PIPELINE_CHECKS).contains(id)) {
			return "PIPELINE";
		} else if (Arrays.asList(COMPLETION_CHECKS).contains(id)) {
			return "COMPLETION";
		} 
		return "OTHER";
	}


	private static Map<String, String> newFailureMap(final CheckInfo checkInfo) {
		
		final Map<String,String> map = new HashMap<>();
		map.put("ID", checkInfo.getId());
		map.put("label", checkInfo.getLabel());
		map.put("imageSessionID", checkInfo.getImageSessionId());
		map.put("imageSessionLabel", checkInfo.getImageSessionLabel());
		map.put("imageSessionDate", (checkInfo.getImageSessionDate()!=null) ?
				CommonConstants.getFormatYYMMDD10().format(checkInfo.getImageSessionDate()) : "");
		map.put("site", LifespanUtils.getLifespanSiteFromScannerInfo(checkInfo.getScanner()));
		map.put("scanner", checkInfo.getScanner());
		map.put("operator", checkInfo.getOperator());
		return map;
		
	}
	
	private static List<FailedScanCheckInfo> getScanCheckInfo(final String projectId, final UserI sessionUser,
			final JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE exp.project='" + projectId + "'";
		final String queryStr = SanityChecksQueries.FAILED_SCAN_CHECK_INFO_QUERY.replace(SanityChecksQueries.WHERE_REPLACE, whereStr);
		final List<FailedScanCheckInfo> scanChecks = jdbcTemplate.query(queryStr,
				new BeanPropertyRowMapper<FailedScanCheckInfo>(FailedScanCheckInfo.class));
		return scanChecks;
	}

	private static List<FailedSessionCheckInfo> getSessionCheckInfo(final String projectId,final UserI sessionUser,
			final JdbcTemplate jdbcTemplate) {
		
		final String whereStr = "WHERE exp.project='" + projectId + "'";
		final String queryStr = SanityChecksQueries.FAILED_SESSION_CHECK_INFO_QUERY.replace(SanityChecksQueries.WHERE_REPLACE, whereStr);
		final List<FailedSessionCheckInfo> scans = jdbcTemplate.query(queryStr,
				new BeanPropertyRowMapper<FailedSessionCheckInfo>(FailedSessionCheckInfo.class));
		return scans;
		
	}

}
