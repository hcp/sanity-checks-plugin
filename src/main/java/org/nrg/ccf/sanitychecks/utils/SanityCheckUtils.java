package org.nrg.ccf.sanitychecks.utils;

import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sanitychecks.abst.AbstractSanityChecker;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.inter.SanityCheckRemoverI;
import org.nrg.ccf.sanitychecks.inter.SanityCheckerI;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.SanSanitychecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.SanSanitychecksCheck;
import org.nrg.xdat.om.SanSanitychecksScanChecks;
import org.nrg.xdat.om.SanSanitychecksScanChecksCheck;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatImagesessiondata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.springframework.beans.BeansException;
import org.springframework.http.HttpStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SanityCheckUtils {
	
	public static final String SESSION_INDICATOR = "SESSION";
	
	public static final String QUALITY_UNUSABLE = "unusable";
	
	public static final String SANITY_CHECK_LABEL_EXTENSION = "_sc";
	
	public static void createNewSanityChecksAssessorAndUpdate(XnatImagesessiondata imageSession, List<SanityCheck> checks, UserI user) throws Exception {
		
		createNewSanityChecksAssessorAndUpdate(imageSession, separateIntoScans(checks), user);
		
	}
	
	public static void updateExistingSanityChecksAssessor(XnatImagesessiondata imageSession, SanSanitychecks sanSanitychecks, List<SanityCheck> checks, UserI user) throws Exception {
		
		updateExistingSanityChecksAssessor(imageSession, sanSanitychecks, separateIntoScans(checks), user);
		
	}

	public static Map<String,List<SanityCheck>> separateIntoScans(List<SanityCheck> checks) {
		final HashMap <String,List<SanityCheck>> scanMap = new HashMap<>();
		for (SanityCheck scheck : checks) {
			String scanNumber = scheck.getScanNumber();
			if (scanNumber == null || scanNumber.trim().length()<1) {
				// Session checks have blank scan number
				scanNumber=SanityCheckUtils.SESSION_INDICATOR;
			}
			if (!scanMap.containsKey(scanNumber)) {
				final List<SanityCheck> scanChecks = new ArrayList<>();
				scanMap.put(scanNumber, scanChecks);
			}
			scanMap.get(scanNumber).add(scheck);
		}
		return scanMap;
	}
	
	public static HttpStatus runChecks(String projId, String subjId, String expId, SanityCheckerI checker, UserI user) throws SanityChecksException {
		
		log.debug("Begin running checks (EXP=" + expId + ")");
		XnatProjectdata proj = null;
		XnatSubjectdata subj = null;
		XnatExperimentdata exp = null;
		XnatImagesessiondata imageSession = null;
		SanSanitychecks sanityChecks = null;
		
		log.debug("Obtaining session");
		if (projId!=null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projId, user, false);
			if (proj == null) {
				log.error("ERROR:  Specified project either does not exist or user is not permitted " +
							"access to project data (ProjID=" + projId + ").");
				return HttpStatus.NOT_FOUND;
			}
			subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjId, user, false);
			if (subj==null) {
				subj = XnatSubjectdata.getXnatSubjectdatasById(subjId, user, false);
			}
			if (subj != null && (proj != null && !subj.hasProject(proj.getId()))) {
				subj = null;
			}
			if (subj == null) {
				log.error("ERROR:  Specified subject either does not exist or user is not permitted access to " +
							"subject data (SubjID=" + subjId + ").");
				return HttpStatus.NOT_FOUND;
			}
			exp = XnatExperimentdata.getXnatExperimentdatasById(expId, user, false);
			if (exp != null && (proj != null && !exp.hasProject(proj.getId()))) {
				exp = null;
			}
			if(exp==null){
				exp = (XnatExperimentdata)XnatExperimentdata.GetExptByProjectIdentifier(proj.getId(),expId, user, false);
			}
			
		} else {
			
			exp = XnatExperimentdata.getXnatExperimentdatasById(expId, user, false);
			proj = exp!=null ? exp.getProjectData() : null;
			
		}
		if (exp==null) {
			log.error("ERROR:  Could not locate specified experiment");
			return HttpStatus.NOT_FOUND;
		}
		try {
			if (!proj.canEdit(user)) { 
				log.error("ERROR:  User account is not permitted to access this service.");
				return HttpStatus.UNAUTHORIZED;
			}
		} catch (Exception e) {
			log.error("INTERNAL ERROR:  Could not evaluate user permissions");
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		List<SanityCheck> checksList = null;
		if (exp instanceof SanSanitychecks) {
			sanityChecks = (SanSanitychecks)exp;
			if (checker instanceof SanityCheckRemoverI && ((SanityCheckRemoverI)checker).hasChecksToRemove()) {
				((SanityCheckRemoverI)checker).removeChecks(sanityChecks, user);
			}
			imageSession = sanityChecks.getImageSessionData();
			log.debug("Begin running checks.");
			checksList = checker.doChecks(projId, imageSession, user);
		} else if (exp instanceof XnatImagesessiondata) {
			imageSession = (XnatImagesessiondata)exp;
			log.debug("Begin running checks.");
			checksList = checker.doChecks(projId, imageSession, user);
			checksList=removeChecks(checksList, checker);
			final ArrayList<XnatImageassessordata> assessors = imageSession.getAssessors(SanSanitychecks.SCHEMA_ELEMENT_NAME);
			if (assessors.isEmpty()) {
				try {
					log.debug("Generating new sanity checks assessor.");
					SanityCheckUtils.createNewSanityChecksAssessorAndUpdate(imageSession, checksList, user);
					return HttpStatus.OK;
				} catch (Exception e) {
					log.error("ERROR: Exception thrown during processing:" ,e);
					return HttpStatus.INTERNAL_SERVER_ERROR;
				} 
			} else if (assessors.size()==1) {
				sanityChecks = (SanSanitychecks)assessors.get(0);
				if (checker instanceof SanityCheckRemoverI && ((SanityCheckRemoverI)checker).hasChecksToRemove()) {
					((SanityCheckRemoverI)checker).removeChecks(sanityChecks, user);
				}
			} else if (assessors.size()>1) {
				log.error("ERROR:  Image session contains multiple sanity checks assessors.  Should have only one.");
				return HttpStatus.CONFLICT;
			}
		}
		if (imageSession == null) {
			log.error("ERROR:  Specified experiment is not an image session or sanity check assessor (ExpID=" + expId + ").");
			return HttpStatus.BAD_REQUEST;
			
		}
		if (sanityChecks == null) {
			log.error("ERROR:  Could not obtain sanity check assessor for experiment (ExpID=" + expId + ").");
			return HttpStatus.INTERNAL_SERVER_ERROR;
			
		}
		if (checksList == null) {
			log.error("ERROR:  Could not generate checks from checkClass (CLASS=" + checker.getClass().getName() + ").");
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		try {
			log.debug("Updating existing sanity checks assessor.");
			SanityCheckUtils.updateExistingSanityChecksAssessor(imageSession, sanityChecks, checksList, user);
			return HttpStatus.OK;
		} catch (Exception e) {
			log.error("ERROR: Exception thrown during processing:" ,e);
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		
	}
	
	/**
	 * Removes the checks.
	 *
	 * @param checksList the checks list
	 * @param checker the checker
	 * @return the list
	 */
	private static List<SanityCheck>  removeChecks(List<SanityCheck> checksList, SanityCheckerI checker) {
		List<SanityCheck> removeChecksList= new ArrayList<SanityCheck>();
		AbstractSanityChecker chk = (AbstractSanityChecker) checker;
		if (chk.hasChecksToRemove()) {
			List<String> sessionLevelChecks = chk.getSessionChecksToRemove();
			List<String> scanLevelChecks = chk.getScanChecksToRemove();

			for (Iterator<SanityCheck> sanityCheckIterator = checksList.iterator(); sanityCheckIterator.hasNext();) {
				SanityCheck sanityCheck = sanityCheckIterator.next();
				if (("").equalsIgnoreCase(sanityCheck.getScanNumber())
						&& sessionLevelChecks.contains(sanityCheck.getCheckShortName())) {
					removeChecksList.add(sanityCheck);
				} else if (!("").equalsIgnoreCase(sanityCheck.getScanNumber())
						&& scanLevelChecks.contains(sanityCheck.getCheckShortName())) {
					removeChecksList.add(sanityCheck);
				}
			}
		}
		checksList.removeAll(removeChecksList);
		return checksList;
	}

	public static SanityCheckerI getSanityChecker(final String checkClass) throws BeansException,ClassNotFoundException,InvalidClassException {
		
		final Object checkClassObj;
		try {
			checkClassObj = XDAT.getContextService().getBean(Class.forName(checkClass));
		} catch (BeansException | ClassNotFoundException e) {
			log.error("ERROR: Requested checkClass, " + checkClass + 
					" was not found either on the ClassPath or in the application context.", e);
			throw e; 
		}
		if (checkClassObj == null || !(checkClassObj instanceof SanityCheckerI)) {
			final String errMsg = "ERROR: Requested checkClass, " + checkClass + 
					" is null or does not extend SanityCheckerI.";
			log.error(errMsg);
			throw new InvalidClassException(errMsg);
		}
		final SanityCheckerI checker = (SanityCheckerI)checkClassObj;
		return checker;
		
	}
	
	private static void createNewSanityChecksAssessorAndUpdate(XnatImagesessiondata imageSession, Map<String, List<SanityCheck>> scanUpdates, UserI user) throws Exception {
		
			final SanSanitychecks schecks = new SanSanitychecks(user);
			schecks.setId(XnatExperimentdata.CreateNewID());
			schecks.setDate(new Date());
			schecks.setLabel(imageSession.getLabel() + SANITY_CHECK_LABEL_EXTENSION);
			schecks.setImagesessionId(imageSession.getId());
			schecks.setProject(imageSession.getProject());
			imageSession.addAssessors_assessor(schecks);
			applyUpdates(imageSession, schecks,scanUpdates, user);
			saveSanityCheckEntry(schecks, user);
		
		
	}

	private static void updateExistingSanityChecksAssessor(XnatImagesessiondata imageSession, SanSanitychecks sanSanitychecks, Map<String, List<SanityCheck>> scanUpdates, UserI user) throws Exception {
		
		applyUpdates(imageSession, sanSanitychecks,scanUpdates, user);
		// Per Greg, sanity checks date should update every time record is checked (reflect the last time was checked).
		sanSanitychecks.setDate(new Date());
		saveSanityCheckEntry(sanSanitychecks, user);
		
	}
	
	private static void applyUpdates(XnatImagesessiondata imageSession, SanSanitychecks schecks, Map<String, List<SanityCheck>> scanUpdates, UserI user) throws Exception {
		
		if (!scanUpdates.containsKey(SESSION_INDICATOR) && schecks.getSession_status() == null) {
			// Set session status to passed if there are no session level checks
			schecks.setSession_status(Outcome.PASSED.toString());
			
		}
		for (final String key : scanUpdates.keySet()) {
			final List<SanityCheck> csvrows = scanUpdates.get(key);
			final List<String> currentIDS = new ArrayList<String>();
			for (final SanityCheck scheck : csvrows) {
				currentIDS.add(scheck.getCheckShortName());
			}	
			for (final SanityCheck scheck : csvrows) {
				final String checkScanID=key;
				final String checkID=scheck.getCheckShortName();
				final String checkDesc=scheck.getCheckLongName();
				final Outcome checkOutcome = scheck.getOutcome();
				final String checkDiag=scheck.getDescriptionOfFailure();
				final String checkAcceptJust=scheck.getAcceptJustification();
				final String checkAcceptUser=scheck.getAcceptUsername();
				
				final String currentSessJust = schecks.getSession_statusoverride_justification();
				final String currentSessUser = schecks.getSession_statusoverride_username();
				
				if (checkScanID.equalsIgnoreCase(SESSION_INDICATOR)) {
					boolean matchingCheck = false;
					for (final SanSanitychecksCheckI check : schecks.getSession_sessionChecks_check()) {
						if (check.getId().equals(checkID)) {
							matchingCheck = true;
							updateStatus(check,checkOutcome ,checkDiag);
							if (checkDesc != null && checkDesc.length()>0) {
								check.setDescription(checkDesc);
							}
							if ((scheck.getScanNumber() == null || scheck.getScanNumber().length()<1) && 
									checkAcceptUser.length()>0 && checkAcceptJust.length()>0 && 
									(currentSessJust == null || currentSessUser == null || currentSessJust.isEmpty() || currentSessUser.isEmpty())) {
								schecks.setSession_statusoverride_justification(checkAcceptJust);
								schecks.setSession_statusoverride_username(checkAcceptUser);
								schecks.setSession_statusoverride_datetime(new Date());
							}
						}
					}
					if (!matchingCheck) {
						final SanSanitychecksCheckI check = new SanSanitychecksCheck(user);
						check.setId(checkID);
						check.setStatus(checkOutcome.toString());
						check.setDescription(checkDesc);
						if (check.getStatus().equalsIgnoreCase(Outcome.FAILED.toString())) {
							check.setDiagnosis(checkDiag);
						} else if (check.getStatus().equalsIgnoreCase(Outcome.WARNING.toString())) {
							check.setDiagnosis(checkDiag);
						}
						if ((scheck.getScanNumber() == null || scheck.getScanNumber().length()<1) && 
								checkAcceptUser.length()>0 && checkAcceptJust.length()>0 && 
								(currentSessJust == null || currentSessUser == null || currentSessJust.isEmpty() || currentSessUser.isEmpty())) {
							schecks.setSession_statusoverride_justification(checkAcceptJust);
							schecks.setSession_statusoverride_username(checkAcceptUser);
							schecks.setSession_statusoverride_datetime(new Date());
						}
						schecks.addSession_sessionChecks_check(check);
					}
				} else {	
					boolean matchingCheck = false;
					SanSanitychecksScanChecksI scan = getChecksForScan(schecks,checkScanID);
					if (scan != null) {
						for (final SanSanitychecksScanChecksCheckI check : scan.getCheck()) {
							if (check.getId().equals(checkID)) {
								matchingCheck = true;
								final String currentJust = scan.getStatusoverride_justification();
								final String currentUser = scan.getStatusoverride_username();
								updateStatus(check,checkOutcome ,checkDiag);
								if (checkDesc != null && checkDesc.length()>0) {
									check.setDescription(checkDesc);
								}
								if (checkAcceptUser.length()>0 && checkAcceptJust.length()>0 && 
										(currentJust == null || currentUser == null || currentJust.isEmpty() || currentUser.isEmpty())) {
									scan.setStatusoverride_username(checkAcceptUser);
									scan.setStatusoverride_justification(checkAcceptJust);
									scan.setStatusoverride_datetime(new Date());
								}
							}
						}
					} else {
						scan =  new SanSanitychecksScanChecks();
						scan.setScanId(checkScanID);
						scan.setStatusoverride_username(checkAcceptUser);
						scan.setStatusoverride_justification(checkAcceptJust);
						scan.setStatusoverride_datetime(new Date());
						schecks.addScans_scanChecks(scan);
					}
					if (!matchingCheck) {
						final SanSanitychecksScanChecksCheckI check = new SanSanitychecksScanChecksCheck(user);
						check.setId(checkID);
						check.setStatus(checkOutcome.toString());
						check.setDescription(checkDesc);
						if (check.getStatus().equalsIgnoreCase(Outcome.FAILED.toString())) {
							check.setDiagnosis(checkDiag);
						} else if (check.getStatus().equalsIgnoreCase(Outcome.WARNING.toString())) {
							check.setDiagnosis(checkDiag);
						}
						final String currentJust = scan.getStatusoverride_justification();
						final String currentUser = scan.getStatusoverride_username();
						if (checkAcceptUser.length()>0 && checkAcceptJust.length()>0 && 
								(currentJust == null || currentUser == null || currentJust.isEmpty() || currentUser.isEmpty())) {
							scan.setStatusoverride_username(checkAcceptUser);
							scan.setStatusoverride_justification(checkAcceptJust);
							scan.setStatusoverride_datetime(new Date());
						}
						scan.addCheck(check);
					}
				}
			}
		}
		updateSessionAndScanAndOverallStatus(schecks, imageSession);
	}
	
	private static void updateSessionAndScanAndOverallStatus(SanSanitychecks schecks, BaseXnatImagesessiondata imageSession) {
		schecks.setSession_status(Outcome.PASSED.toString());
		boolean anyFailed = false;
		boolean anyWarning = false;
		boolean anyAccepted = false;
		for (final SanSanitychecksCheckI check : schecks.getSession_sessionChecks_check()) {
			if (schecks.getSession_status().equals(Outcome.FAILED.toString())) {
				continue;
			}
			if (check.getStatus().equals(Outcome.FAILED.toString())) {
				anyFailed = true;
				schecks.setSession_status(Outcome.FAILED.toString());
			} else if (check.getStatus().equals(Outcome.WARNING.toString())) {
				anyWarning = true;
				schecks.setSession_status(Outcome.WARNING.toString());
			} else if (check.getStatus().equals(Outcome.ACCEPTED.toString()) && schecks.getSession_status().equals(Outcome.PASSED.toString())) {
				anyAccepted = true;
				schecks.setSession_status(Outcome.ACCEPTED.toString());
			}
		}
		final List<SanSanitychecksScanChecksI> scans = schecks.getScans_scanChecks();
		for (SanSanitychecksScanChecksI scan : scans) {
			final XnatImagescandata mrScan = imageSession.getScanById(scan.getScanId());
			if (mrScan == null) {
				// In case we've removed the scan from the session.
				continue;
			}
			if (mrScan.getQuality() != null && mrScan.getQuality().equalsIgnoreCase(QUALITY_UNUSABLE)) {
				// Only set status here (required for SQL constraints).  Don't want to include in overall status and if usability changes,
				// we want indicator in place that the scan was unusable when sanity checks were run.
				scan.setStatus(Outcome.SCAN_UNUSABLE.toString());
				continue;
			}
			scan.setStatus(Outcome.PASSED.toString());
			for (final SanSanitychecksScanChecksCheckI check : scan.getCheck()) {
				if (scan.getStatus().equals(Outcome.FAILED.toString())) {
					continue;
				}
				if (check.getStatus().equals(Outcome.FAILED.toString())) {
					anyFailed = true;
					scan.setStatus(Outcome.FAILED.toString());
				} else if (check.getStatus().equals(Outcome.WARNING.toString())) {
					anyWarning = true;
					scan.setStatus(Outcome.WARNING.toString());
				} else if (check.getStatus().equals(Outcome.ACCEPTED.toString()) && scan.getStatus().equals(Outcome.PASSED.toString())) {
					anyAccepted = true;
					scan.setStatus(Outcome.ACCEPTED.toString());
				}
			}
		}
		if (anyFailed) {
			schecks.setOverall_status(Outcome.FAILED.toString());
		} else if (anyWarning) {
			schecks.setOverall_status(Outcome.WARNING.toString());
		} else if (anyAccepted) {
			schecks.setOverall_status(Outcome.ACCEPTED.toString());
		} else {
			schecks.setOverall_status(Outcome.PASSED.toString());
		}
	}
	
	private static void updateStatus(SanSanitychecksCheckI check, Outcome checkOutcome, String checkDiag) {
		
		if (checkOutcome.equals(Outcome.PASSED)) {
			check.setStatus(Outcome.PASSED.toString());
			check.setDiagnosis(null);
		} else if (checkOutcome.equals(Outcome.FAILED)) {
			check.setDiagnosis(checkDiag);
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(Outcome.ACCEPTED.toString())) {
				check.setStatus(Outcome.FAILED.toString());
			}
		} else if (checkOutcome.equals(Outcome.WARNING)) {
			check.setDiagnosis(checkDiag);
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(Outcome.ACCEPTED.toString())) {
				check.setStatus(Outcome.WARNING.toString());
			}
		} else if (checkOutcome.equals(Outcome.ACCEPTED)) {
			if (check.getStatus().equals(Outcome.FAILED.toString())) {
				check.setStatus(Outcome.ACCEPTED.toString());
				check.setDiagnosis(null);
			}
		}
		
	}
	
	private static void updateStatus(SanSanitychecksScanChecksCheckI check, Outcome checkOutcome, String checkDiag) {
		
		if (checkOutcome.equals(Outcome.PASSED)) {
			check.setStatus(Outcome.PASSED.toString());
			check.setDiagnosis(null);
		} else if (checkOutcome.equals(Outcome.FAILED)) {
			check.setDiagnosis(checkDiag);
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(Outcome.ACCEPTED.toString())) {
				check.setStatus(Outcome.FAILED.toString());
			}
		} else if (checkOutcome.equals(Outcome.WARNING)) {
			check.setDiagnosis(checkDiag);
			// TO DO: Should we compare diagnoses here?????
			if (!check.getStatus().equals(Outcome.ACCEPTED.toString())) {
				check.setStatus(Outcome.WARNING.toString());
			}
		} else if (checkOutcome.equals(Outcome.ACCEPTED)) {
			if (check.getStatus().equals(Outcome.FAILED.toString())) {
				check.setStatus(Outcome.ACCEPTED.toString());
				check.setDiagnosis(null);
			}
		}
		
	}
	
	private static void saveSanityCheckEntry(SanSanitychecks schecks, UserI user) throws Exception {
		
		final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, schecks.getItem(),
				EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_VIA_WEB_SERVICE, null, null));
		final EventMetaI ci = wrk.buildEvent();
		if (SaveItemHelper.authorizedSave(schecks,user,false,true,ci)) {
			PersistentWorkflowUtils.complete(wrk, ci);
		} else {
			PersistentWorkflowUtils.fail(wrk,ci);
		}
		
	}
	


	private static SanSanitychecksScanChecksI getChecksForScan(SanSanitychecks schecks, String checkScanID) {
		final List<SanSanitychecksScanChecksI> scans = schecks.getScans_scanChecks();
		for (SanSanitychecksScanChecksI scan : scans) {
			if (scan.getScanId().equalsIgnoreCase(checkScanID)) {
				return scan;
			}
		}
		return null;
	}

}
