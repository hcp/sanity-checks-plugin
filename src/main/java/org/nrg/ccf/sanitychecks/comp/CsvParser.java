package org.nrg.ccf.sanitychecks.comp;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;

import au.com.bytecode.opencsv.CSVReader;

/**
 * The Class CsvParser.
 */
public class CsvParser {
	
	/**
	 * Parses the csv.
	 *
	 * @param fileName the file name
	 * @return the list
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static List<SanityCheck> parseCSV(String fileName) throws IOException{
		return parseCSV(new CSVReader(new FileReader(fileName)));
	}
	
	/**
	 * Parses the csv.
	 *
	 * @param inFile the in file
	 * @return the list
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static List<SanityCheck> parseCSV(File inFile) throws IOException{
		return parseCSV(new CSVReader(new FileReader(inFile)));
	}
	
	/**
	 * Parses the csv contents.
	 *
	 * @param fileContents the file contents
	 * @return the list
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static List<SanityCheck> parseCSVContents(String fileContents) throws IOException{
		return parseCSV(new CSVReader(new StringReader(fileContents)));
	}
	
	/**
	 * Converts the CSV file, in which the columns represent the metrics and the rows 
	 * represent the scans, into a list of maps.  Each map in this list represents 
	 * one row from the file.  The map is keyed by the column header and holds the 
	 * value for that row at that column.
	 *
	 * @param csvReader the csv reader
	 * @return the list
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	 static List<SanityCheck> parseCSV(CSVReader csvReader) throws IOException{
		ArrayList<SanityCheck> rows = new ArrayList<>();
		List<String[]> lines = csvReader.readAll();
        String[] header;
        int startIndex;
		if (lines.size() >= 2) {
            if (lines.get(0)[0].contains("Study Name:")) { // Indicates Toolbox export
                header = lines.get(2);
                startIndex = 3;
            } else {
			    header = lines.get(0);
                startIndex = 1;
            }
			for (int i=startIndex; i<lines.size(); i++) {
				String[] line = lines.get(i);
				SanityCheck scheck=new SanityCheck();
				for (int j=0;j<line.length;j++) {
					final String key = header[j];
					final String value = line[j];
					if (value == null || value.trim().length()<1) {
						continue;
					}
					if (key.equalsIgnoreCase("SessionID")) {
						scheck.setSessionId(value);
					} else if (key.equalsIgnoreCase("ScanNumber")) {
						scheck.setScanNumber(value);
					} else if (key.equalsIgnoreCase("SeriesDesc")) {
						scheck.setSeriesDesc(value);
					} else if (key.equalsIgnoreCase("CheckShortName")) {
						scheck.setCheckShortName(value);
					} else if (key.equalsIgnoreCase("CheckLongName")) {
						scheck.setCheckLongName(value);
					} else if (key.equalsIgnoreCase("Outcome") || key.equalsIgnoreCase("Pass_Fail")) {
						scheck.setOutcome(valueToOutcome(value));
					} else if (key.equalsIgnoreCase("DescriptionOfFailure") || key.equalsIgnoreCase("Description_of_Failure")) {
						scheck.setDescriptionOfFailure(value);
					}
				}
				rows.add(scheck);
			}
		}
		return rows;

	}

	private static Outcome valueToOutcome(String value) {
		if (value.toUpperCase().startsWith("PASS")) {
			return Outcome.PASSED;
		} else if (value.toUpperCase().startsWith("FAIL")) {
			return Outcome.FAILED;
		} else if (value.toUpperCase().startsWith("WARN")) {
			return Outcome.WARNING;
		}
		return null;
	}
}
