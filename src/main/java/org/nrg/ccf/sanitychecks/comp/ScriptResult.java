package org.nrg.ccf.sanitychecks.comp;

import java.util.List;

/**
 * The Class ScriptResult.
 */
public class ScriptResult {
	
   	/** The status. */
	   private boolean status; 
	
	/** The return code. */
	private Integer returnCode; 
	
	/** The result list. */
	private List<String> resultList;
	
	/**
	 * Instantiates a new script result.
	 */
	public ScriptResult() {
	}
	
	/**
	 * Instantiates a new script result.
	 *
	 * @param status the status
	 * @param returnCode the return code
	 * @param resultList the result list
	 */
	public ScriptResult(boolean status, Integer returnCode, List<String> resultList) {
		setStatus(status);
		setReturnCode(returnCode);
		setResultList(resultList);
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * Sets the return code.
	 *
	 * @param returnCode the new return code
	 */
	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}
	
	/**
	 * Sets the result list.
	 *
	 * @param ResultList the new result list
	 */
	public void setResultList(List<String> ResultList) {
		this.resultList = ResultList;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public boolean getStatus() {
		return status;
	}
	
	/**
	 * Gets the return code.
	 *
	 * @return the return code
	 */
	public Integer getReturnCode() {
		return returnCode;
	}
	
	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<String> getResultList() {
		return resultList;
	}
	
}
