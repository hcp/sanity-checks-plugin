package org.nrg.ccf.sanitychecks.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "sanityChecksPlugin",
			name = "Sanity Checks Plugin"
		)
@ComponentScan({ 
	"org.nrg.ccf.sanitychecks.xapi",
	"org.nrg.ccf.sanitychecks.services",
	"org.nrg.ccf.sanitychecks.components"
	})
public class SanityChecksPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(SanityChecksPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public SanityChecksPlugin() {
		logger.info("Configuring sanity checks plugin");
	}
	
}