package org.nrg.ccf.sanitychecks.components;

import java.util.Date;

public class CheckInfo {
	
	private String id;
	private String project;
	private String label;
	private String imageSessionId;
	private String imageSessionLabel;
	private Date imageSessionDate;
	private String scanner;
	private String operator;
	private String overallStatus;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getProject() {
		return project;
	}
	
	public void setProject(String project) {
		this.project = project;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getImageSessionId() {
		return imageSessionId;
	}
	
	public void setImageSessionId(String imageSessionId) {
		this.imageSessionId = imageSessionId;
	}
	
	public String getImageSessionLabel() {
		return imageSessionLabel;
	}
	
	public void setImageSessionLabel(String imageSessionLabel) {
		this.imageSessionLabel = imageSessionLabel;
	}
	
	public Date getImageSessionDate() {
		return imageSessionDate;
	}
	
	public void setImageSessionDate(Date imageSessionDate) {
		this.imageSessionDate = imageSessionDate;
	}
	
	public String getScanner() {
		return scanner;
	}
	
	public void setScanner(String scanner) {
		this.scanner = scanner;
	}
	
	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public String getOverallStatus() {
		return overallStatus;
	}
	
	public void setOverallStatus(String overallStatus) {
		this.overallStatus = overallStatus;
	}

}
