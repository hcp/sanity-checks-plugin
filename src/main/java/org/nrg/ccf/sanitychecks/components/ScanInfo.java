package org.nrg.ccf.sanitychecks.components;

public class ScanInfo {
	
	private String id;
	private String imageSessionId;
	private String type;
	private String seriesDescription;
	private String quality;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getImageSessionId() {
		return imageSessionId;
	}
	
	public void setImageSessionId(String imageSessionId) {
		this.imageSessionId = imageSessionId;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSeriesDescription() {
		return seriesDescription;
	}
	
	public void setSeriesDescription(String seriesDescription) {
		this.seriesDescription = seriesDescription;
	}
	
	public String getQuality() {
		return quality;
	}
	
	public void setQuality(String quality) {
		this.quality = quality;
	}

}
