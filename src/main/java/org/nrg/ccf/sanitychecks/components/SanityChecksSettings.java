package org.nrg.ccf.sanitychecks.components;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.ccf.sanitychecks.constants.CheckerType;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = SanityChecksSettings.TOOL_ID, toolName = "Sanity Checks Settings")
public class SanityChecksSettings extends AbstractProjectPreferenceBean {

	private static final long serialVersionUID = 4845471859401508875L;
	protected static final String TOOL_ID = "sanityChecksSettings";

	@Autowired
	protected SanityChecksSettings(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
	}
	
	@NrgPreference
	public Boolean getEnabled() {
		return false;
	}

	public Boolean getEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, "enabled");
	}

	public void setEnabled(final String entityId, final Boolean enabled) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		try {
			this.setBooleanValue(SCOPE, entityId, enabled, "enabled");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}
	
	@NrgPreference
	public CheckerType getCheckerType() {
		return null;
	}

	public CheckerType getCheckerType(final String entityId) {
		return this.getObjectValue(SCOPE, entityId, "checkerType",CheckerType.class);
	}

	public void setCheckerType(final String entityId, final CheckerType checkerType) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		try {
			this.setObjectValue(SCOPE, entityId, checkerType, "checkerType");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}
	
	@NrgPreference
	public String getChecksClass() {
		return "";
	}

	public String getChecksClass(final String entityId) {
		return this.getValue(SCOPE, entityId, "checksClass");
	}

	public void setChecksClass(final String entityId, final String checksClass) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		try {
			this.set(SCOPE, entityId, checksClass, "checksClass");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getScriptId() {
		return "";
	}
	
	public String getScriptId(final String entityId) {
		return this.getValue(SCOPE, entityId, "scriptId");
	}

	public void setScriptId(final String entityId, final String scriptId) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		try {
			this.set(SCOPE, entityId, scriptId, "scriptId");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	public SanityChecksSettingsInfo getInfo(String projectId) {
		final SanityChecksSettingsInfo settings = new SanityChecksSettingsInfo();
		settings.setEnabled(this.getEnabled(projectId));
		settings.setCheckerType(this.getCheckerType(projectId));
		settings.setChecksClass(this.getChecksClass(projectId));
		settings.setScriptId(this.getScriptId(projectId));
		return settings;
	}

}
