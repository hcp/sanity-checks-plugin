package org.nrg.ccf.sanitychecks.components;

import org.nrg.ccf.sanitychecks.constants.CheckerType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SanityChecksSettingsInfo {
	
	private Boolean enabled;
	private CheckerType checkerType;
	private String checksClass;
	private String scriptId;
	
}
