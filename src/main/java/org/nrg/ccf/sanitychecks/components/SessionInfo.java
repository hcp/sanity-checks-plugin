package org.nrg.ccf.sanitychecks.components;

public class SessionInfo {
	
	private String id;
	private String label;
	private String scanner;
	private String operator;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getScanner() {
		return scanner;
	}
	
	public void setScanner(String scanner) {
		this.scanner = scanner;
	}
	
	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	} 

}
