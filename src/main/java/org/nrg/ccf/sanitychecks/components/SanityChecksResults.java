package org.nrg.ccf.sanitychecks.components;

import org.nrg.automation.entities.ScriptOutput;
import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SanityChecksResults {
	
	private HttpStatus httpStatus;
	private ScriptOutput scriptOutput;
	private String runInfo = "";

}
