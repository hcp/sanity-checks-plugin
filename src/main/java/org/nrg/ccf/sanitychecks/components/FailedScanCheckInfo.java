package org.nrg.ccf.sanitychecks.components;

public class FailedScanCheckInfo extends CheckInfo {
	
	private String numScanId;
	private String scanId;
	private String seriesDescription;
	private String scanType;
	private String scanQuality;
	private String scanner;
	private String scanStatus;
	private String status;
	private String description;
	private String diagnosis;
	private String checkId;
	
	public String getNumScanId() {
		return numScanId;
	}
	
	public void setNumScanId(String numScanId) {
		this.numScanId = numScanId;
	}
	
	public String getScanId() {
		return scanId;
	}
	
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	
	public String getSeriesDescription() {
		return seriesDescription;
	}
	
	public void setSeriesDescription(String seriesDescription) {
		this.seriesDescription = seriesDescription;
	}
	
	public String getScanType() {
		return scanType;
	}
	
	public void setScanType(String scanType) {
		this.scanType = scanType;
	}
	
	public String getScanQuality() {
		return scanQuality;
	}
	
	public void setScanQuality(String scanQuality) {
		this.scanQuality = scanQuality;
	}
	
	public String getScanner() {
		return scanner;
	}
	
	public void setScanner(String scanner) {
		this.scanner = scanner;
	}
	
	public String getScanStatus() {
		return scanStatus;
	}
	
	public void setScanStatus(String scanStatus) {
		this.scanStatus = scanStatus;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDiagnosis() {
		return diagnosis;
	}
	
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	public String getCheckId() {
		return checkId;
	}
	
	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}

}
