package org.nrg.ccf.sanitychecks.components;

public class FailedSessionCheckInfo extends CheckInfo {
	
	private String sessionStatus;
	private String status;
	private String description;
	private String diagnosis;
	private String checkId;
	
	public String getSessionStatus() {
		return sessionStatus;
	}
	
	public void setSessionStatus(String sessionStatus) {
		this.sessionStatus = sessionStatus;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDiagnosis() {
		return diagnosis;
	}
	
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	public String getCheckId() {
		return checkId;
	}
	
	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}

}
