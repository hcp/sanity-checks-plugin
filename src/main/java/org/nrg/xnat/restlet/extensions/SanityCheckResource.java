package org.nrg.xnat.restlet.extensions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.nrg.action.ClientException;
import org.nrg.automation.entities.ScriptOutput;
import org.nrg.ccf.sanitychecks.comp.CsvParser;
import org.nrg.ccf.sanitychecks.components.SanityChecksResults;
import org.nrg.ccf.sanitychecks.dto.Outcome;
import org.nrg.ccf.sanitychecks.dto.SanityCheck;
import org.nrg.ccf.sanitychecks.exception.SanityChecksException;
import org.nrg.ccf.sanitychecks.exception.SanityChecksScriptRunException;
import org.nrg.ccf.sanitychecks.services.SanityChecksService;
import org.nrg.ccf.sanitychecks.utils.SanityCheckUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.SanSanitychecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksCheckI;
import org.nrg.xdat.model.SanSanitychecksScanChecksI;
import org.nrg.xdat.om.SanSanitychecks;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImageassessordata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.springframework.http.HttpStatus;

/**
 * The Class SanityCheckResource.
 */
@XnatRestlet({	
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SCAN_ACCEPTCHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SCAN_ACCEPTCHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SESSION_ACCEPTCHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.SESSION_ACCEPTCHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.MARK_SCANS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.MARK_SCANS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.OVERALL_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.OVERALL_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CALCULATE_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CALCULATE_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.UPDATE_STATUS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.UPDATE_STATUS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CSV_UPDATE_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.CSV_UPDATE_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.RUN_CHECKS_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.RUN_CHECKS_SEGMENT,
			"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.EMAIL_ON_RUN_SEGMENT,
			"/experiments/{EXP_ID}/sanitychecks/" + SanityCheckResource.EMAIL_ON_RUN_SEGMENT,
			})
public class SanityCheckResource extends SecureResource {
	
	/** The Constant CHECK_ID. */
	private static final String CHECK_ID = "CheckShortName";
	
	/** The Constant CHECK_DESC. */
	private static final String CHECK_DESC = "CheckLongName";
	
	/** The Constant CHECK_STATUS. */
	private static final String CHECK_STATUS = "Pass_Fail";
	
	/** The Constant CHECK_DIAG. */
	private static final String CHECK_DIAG = "Description_of_Failure";
	
	/** The Constant csv_columns. */
	public static final String[] csv_columns = { "SessionID","ScanNumber","SeriesDesc", CHECK_ID, CHECK_DESC, CHECK_STATUS, CHECK_DIAG };
	
	/** The Constant SCAN_ACCEPTCHECKS_SEGMENT. */
	public static final String SCAN_ACCEPTCHECKS_SEGMENT = "acceptScanChecks";
	
	/** The Constant SESSION_ACCEPTCHECKS_SEGMENT. */
	public static final String SESSION_ACCEPTCHECKS_SEGMENT = "acceptSessionChecks";
	
	/** The Constant MARK_SCANS_SEGMENT. */
	public static final String MARK_SCANS_SEGMENT = "markScansUnusable";
	
	/** The Constant OVERALL_STATUS_SEGMENT. */
	public static final String OVERALL_STATUS_SEGMENT = "getOverallStatus";
	
	/** The Constant CALCULATE_STATUS_SEGMENT. */
	public static final String CALCULATE_STATUS_SEGMENT = "calculateOverallStatus";
	
	/** The Constant UPDATE_STATUS_SEGMENT. */
	public static final String UPDATE_STATUS_SEGMENT = "updateOverallStatus";
	
	/** The Constant CSV_UPDATE_SEGMENT. */
	public static final String CSV_UPDATE_SEGMENT = "updateViaCSV";
	
	/** The Constant RUN_CHECKS_SEGMENT. */
	public static final String RUN_CHECKS_SEGMENT = "runSanityChecks";
	
	/** The Constant EMAIL_ON_RUN_SEGMENT. */
	public static final String EMAIL_ON_RUN_SEGMENT = "emailOnRun";
	
	/** The proj. */
	private XnatProjectdata proj = null;
	
	/** The subj. */
	private XnatSubjectdata subj = null;
	
	/** The exp. */
	private XnatExperimentdata exp = null;
	
	/** The sanity checks. */
	private SanSanitychecks sanityChecks = null;
	
	/** The image session. */
	private XnatImagesessiondata imageSession = null;
	
	/** The Constant sessionEmailList. */
	private static final List<String> sessionEmailList = new ArrayList<String>();
	
	/** The fwlist. */
	private List<FileWriterWrapperI> fwlist;
	
	/** The logger. */
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SanityCheckResource.class);

	/**
	 * Instantiates a new sanity check resource.
	 *
	 * @param context the context
	 * @param request the request
	 * @param response the response
	 */
	public SanityCheckResource(Context context, Request request, Response response) {
		super(context, request, response);

		final String projID = (String)getParameter(request,"PROJECT_ID");
		final String subjID = (String)getParameter(request,"SUBJECT_ID");
		final String exptID = (String)getParameter(request,"EXP_ID");
		if (projID!=null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projID, getUser(), false);
			if (proj == null) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified project either does not exist or user is not permitted access to subject data");
				return;
			}
			subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjID, getUser(), false);
			if (subj==null) {
				subj = XnatSubjectdata.getXnatSubjectdatasById(subjID, getUser(), false);
			}
			if (subj != null && (proj != null && !subj.hasProject(proj.getId()))) {
				subj = null;
			}
			if (subj == null) {
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified subject either does not exist or user is not permitted access to subject data");
			}
			exp = XnatExperimentdata.getXnatExperimentdatasById(exptID, getUser(), false);
			if (exp != null && (proj != null && !exp.hasProject(proj.getId()))) {
				exp = null;
			}
			if(exp==null){
				exp = (XnatExperimentdata)XnatExperimentdata.GetExptByProjectIdentifier(proj.getId(),exptID, getUser(), false);
			}
			
		} else {
			
			exp = XnatExperimentdata.getXnatExperimentdatasById(exptID, getUser(), false);
			proj = exp!=null ? exp.getProjectData() : null;
			
		}
		if (exp==null) {
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND,"ERROR:  Could not locate specified experiment");
			return;
		}
		try {
			if (!proj.canEdit(getUser())) { 
				response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED,"User account is not permitted to access this service");
				return;
			}
		} catch (Exception e) {
			response.setStatus(Status.SERVER_ERROR_INTERNAL,"INTERNAL ERROR:  Could not evaluate user permissions");
			return;
		}
		if (exp instanceof SanSanitychecks) {
			sanityChecks = (SanSanitychecks)exp;
			imageSession = sanityChecks.getImageSessionData();
		} else if (exp instanceof XnatImagesessiondata) {
			imageSession = (XnatImagesessiondata)exp;
		}
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#allowGet()
	 */
	@Override
	public boolean allowGet() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#handleGet()
	 */
	@Override
	public void handleGet() {
		final List<String> segments = getRequest().getResourceRef().getSegments();
		if (segmentCount(segments)>1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Invalid URL");
			return;
		}
		if (segments.contains(OVERALL_STATUS_SEGMENT)) {
			getOverallStatus();
		} else if (segments.contains(CALCULATE_STATUS_SEGMENT)) {
			calculateOverallStatus(true);
		}
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#allowPost()
	 */
	@Override
	public boolean allowPost() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#handlePost()
	 */
	@Override
	public void handlePost() {
		final String[] scanA = hasQueryVariable("scanList") ? getQueryVariable("scanList").toString().split(",") : new String[]{};
		final List<String> segments = getRequest().getResourceRef().getSegments();
		if (segmentCount(segments) != 1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Invalid URL");
			return;
		}
		if (segments.contains(SCAN_ACCEPTCHECKS_SEGMENT)) {
			if (validateType(SanSanitychecks.class)) {
				acceptScanChecks(scanA);
			}
		} else if (segments.contains(SESSION_ACCEPTCHECKS_SEGMENT)) {
			if (validateType(SanSanitychecks.class)) {
				acceptSessionChecks();
			}
		} else if (segments.contains(MARK_SCANS_SEGMENT)) {
			if (validateType(SanSanitychecks.class)) {
				markScansUnusable(scanA);
			}
		} else if (segments.contains(UPDATE_STATUS_SEGMENT)) {
			if (validateType(SanSanitychecks.class)) {
				updateOverallStatusDoSave();
			}
		} else if (segments.contains(CSV_UPDATE_SEGMENT)) {
			if (validateType(XnatMrsessiondata.class)) {
				processUploadedFile();
			}
		} else if (segments.contains(EMAIL_ON_RUN_SEGMENT)) {
			if (!sessionEmailList.contains(imageSession.getId())) {
				sessionEmailList.add(imageSession.getId());
			}
			getResponse().setStatus(Status.SUCCESS_OK);
		} else if (segments.contains(RUN_CHECKS_SEGMENT)) {
			
			SanityChecksResults result = null;
			try {
				final SanityChecksService _service = XDAT.getContextService().getBean(SanityChecksService.class);
				if (exp.getClass().isAssignableFrom(XnatMrsessiondata.class)) {
					result = _service.runSanityChecks(getUser(), proj.getId(), (XnatMrsessiondata) exp);
				} else if (exp.getClass().isAssignableFrom(SanSanitychecks.class)) {
					result = _service.runSanityChecks(getUser(), proj.getId(), (XnatMrsessiondata) imageSession);
				}
				if (result != null) {
					setStatusBasedOnResult(result);
				}
			} catch (SanityChecksScriptRunException | SanityChecksException e) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, e);
			}
			sendUserEmail();
			
		}
	}
	
	/**
	 * Send user email.
	 */
	private void sendUserEmail() {
		if (sessionEmailList.contains(imageSession.getId())) {
			sessionEmailList.remove(imageSession.getId());
			final org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
  	      	cc.addClause("san:SanityChecks.imageSession_ID",imageSession.getId());
  	      	cc.addClause("san:SanityChecks.project",proj.getId());
			final ArrayList<SanSanitychecks> schecks = SanSanitychecks.getSanSanitycheckssByField(cc, getUser(), false);
			if (schecks != null && schecks.size()>0) {
				try {
					final SanSanitychecks scheck = schecks.get(0);
					final StringBuilder returnSB=new StringBuilder();
						returnSB.append("<h3>Sanity Check Status:  ")
							.append(scheck.getOverall_status())
							.append("</h3>") 
							.append("<h3>MR Session:  <a href=\"") 
							.append(TurbineUtils.GetFullServerPath()) 
							.append("/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/") 
							.append(imageSession.getId()) 
							.append("/project/") 
							.append(proj.getId()) 
							.append("\">") 
							.append(imageSession.getLabel()) 
							.append("</a></h3>\n") 
							.append("<h3>Sanity Check Session:  <a href=\"") 
							.append(TurbineUtils.GetFullServerPath()) 
							.append("/app/action/DisplayItemAction/search_element/san:sanityChecks/search_field/san:sanityChecks.ID/search_value/") 
							.append(scheck.getId()) 
							.append("/project/") 
							.append(proj.getId()) 
							.append("\">") 
							.append(scheck.getLabel()) 
							.append("</a></h3>\n");
					AdminUtils.sendUserHTMLEmail("Sanity Check run is complete (SESSION=" + exp.getLabel() + ")", returnSB.toString(), false, new String[] { getUser().getEmail() });
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Sets the status based on script result.
	 *
	 * @param result the new status based on script result
	 */
	private void setStatusBasedOnResult(SanityChecksResults result) {
		if (result.getHttpStatus()!=null) {
			final HttpStatus status = result.getHttpStatus();
			getResponse().setStatus(new Status(status.value()),status.getReasonPhrase());
		} else if (result.getScriptOutput()!=null) {
			final ScriptOutput scriptOut = result.getScriptOutput();
			final org.nrg.automation.entities.ScriptOutput.Status status = scriptOut.getStatus();
			final String statusName = status.name();
			final String errorOut = scriptOut.getErrorOutput();
			final String out = scriptOut.getOutput();
			final String detail = (scriptOut.getResults()!=null) ? scriptOut.getResults().toString() : null;
			if (statusName.equals(org.nrg.automation.entities.ScriptOutput.Status.SUCCESS.toString())) {
				getResponse().setStatus(Status.SUCCESS_OK, "Sanity checks updated");
			} else {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
						StringEscapeUtils.escapeHtml("PROCESSING ERROR STATUS=" + statusName + "<br><br>" + out +
								((errorOut != null && errorOut.length()>0) ? "<br>" + errorOut : "") +
								((detail != null && detail.length()>0) ? "<br>" + detail : "")));
			}
		} else {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  No results were returned");
		}
	}

	/**
	 * Segment count.
	 *
	 * @param segments the segments
	 * @return the int
	 */
	private int segmentCount(List<String> segments) {
		return bool2int(segments.contains(SCAN_ACCEPTCHECKS_SEGMENT))+ bool2int(segments.contains(SESSION_ACCEPTCHECKS_SEGMENT))+
				bool2int(segments.contains(MARK_SCANS_SEGMENT))+bool2int(segments.contains(OVERALL_STATUS_SEGMENT))+
				bool2int(segments.contains(CALCULATE_STATUS_SEGMENT))+
				bool2int(segments.contains(UPDATE_STATUS_SEGMENT))+bool2int(segments.contains(CSV_UPDATE_SEGMENT))+
				bool2int(segments.contains(RUN_CHECKS_SEGMENT))+
				bool2int(segments.contains(EMAIL_ON_RUN_SEGMENT))
				;
	}

	/**
	 * Bool2int.
	 *
	 * @param b the b
	 * @return the int
	 */
	private int bool2int(boolean b) {
		return b ? 1 : 0;
	}
	
	/**
	 * Validate type.
	 *
	 * @param c the c
	 * @return true, if successful
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean validateType(Class c) {
		if (!(exp.getClass().isAssignableFrom(c))) {
			final StringBuilder rMsg = new StringBuilder("ERROR:  Experiment is wrong type for this URL");
			if (c.isAssignableFrom(XnatMrsessiondata.class)) {
				rMsg.append(" (Should be xnat:mrSessiondata)");
			} else if (c.isAssignableFrom(SanSanitychecks.class)) {
				rMsg.append(" (Should be san:sanityChecks)");
			} 
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,rMsg.toString());
			return false;
		}
		return true;
	}
	
	/**
	 * Process uploaded file.
	 */
	private void processUploadedFile() {

		final Representation entity = getRequest().getEntity();
		try {
			fwlist=this.getFileWritersAndLoadParams(entity,false);
		} catch (ClientException e1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process uploaded file.");
			return;
		} catch (FileUploadException e1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process uploaded file.");
			return;
		}
		if (fwlist.size() == 0) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  No CSV file received.");
			return;
		} else if ( fwlist.size() > 1) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  May upload only one file per request.");
			return;
		} 
		final FileWriterWrapperI fw = fwlist.get(0);


		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		final Date d = Calendar.getInstance().getTime();
		final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
		final String uploadID = formatter.format(d);
			
		// Save input file to cache space
		cachePath+="user_uploads/"+getUser().getID() + "/" + uploadID + "/";
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
			
		final String fileName = fw.getName();
		//String writeout = null;
		final File cacheFile = new File(cacheLoc,fileName);
		try {
			//StringWriter writer = new StringWriter();
			final FileWriter writer = new FileWriter(cacheFile);
			IOUtils.copy(fw.getInputStream(), writer);
			writer.close();
		} catch (IOException e) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"SERVER ERROR:  Unable to process CSV file.");
			return;
		}
		
		processCSV(cacheFile);
		
	}

	/**
	 * Process csv.
	 *
	 * @param cacheFile the cache file
	 */
	private void processCSV(File cacheFile) {
		
		final List<SanityCheck> csvRep;
		try {
			csvRep = CsvParser.parseCSV(cacheFile);
		} catch (IOException e) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Unable to parse CSV file.  Please check fild and verify it's in CSV format.");
			return;
		}
		if (csvRep==null || csvRep.size()<1) {
			// Let's go ahead and process empty CSV files so an empty data type record can be generated
			//getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Uploaded file contains no data");
			//return;
		} else if (!validateCSVFile(csvRep)) {
			return;
		}
		
		//final Map<String,List<SanityCheck>> scanUpdates = SanityCheckUtils.separateIntoScans(csvRep);
		//SanSanitychecks checks = SanSanitychecks.
		final ArrayList<XnatImageassessordata> assessors = imageSession.getAssessors(SanSanitychecks.SCHEMA_ELEMENT_NAME);
		if (assessors.isEmpty()) {
			//if (SanityCheckUtils.createNewSanityChecksAssesorAndUpdate(scanUpdates)) {
			try {
				SanityCheckUtils.createNewSanityChecksAssessorAndUpdate(imageSession, csvRep, getUser());
				getResponse().setStatus(Status.SUCCESS_OK,"New sanity check assessor has been created");
			} catch (Exception e) {
				e.printStackTrace();
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Exception encountered creating new sanity check assessor (" + e.toString() + ").");
			} 
		} else if (assessors.size()==1) {
			//if (SanityCheckUtils.updateExistingSanityChecksAssessor((SanSanitychecks)assessors.get(0),scanUpdates)) {
			try {
				SanityCheckUtils.updateExistingSanityChecksAssessor(imageSession, (SanSanitychecks)assessors.get(0),csvRep, getUser());
				getResponse().setStatus(Status.SUCCESS_OK,"Sanity check assessor for experiment has been updated");
			} catch (Exception e) {
				e.printStackTrace();
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Exception encountered creating new sanity check assessor (" + e.toString() + ").");
			}
		} else if (assessors.size()>1) {
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"ERROR:  Image sessions contains multiple sanity checks assessors.  Should only have one.");
			return;
		}
	
	}

	/**
	 * Validate csv file.
	 *
	 * @param csvRep the csv rep
	 * @return true, if successful
	 */
	private boolean validateCSVFile(List<SanityCheck> csvRep) {
		for (SanityCheck scheck : csvRep) {
			final String sessionID  = scheck.getSessionId();
			final String scanID  = scheck.getScanNumber();
			if (scheck.getSessionId().equals("") || scheck.getCheckShortName().equals("") ||
					scheck.getCheckLongName().equals("") || scheck.getOutcome() == null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  One or more rows contains an invalid/incomplete check");
				return false;
			}
			if (sessionID == null || !sessionID.equals(imageSession.getLabel())) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  CSV file should contain data for and ONLY contain data for current session (" + imageSession.getLabel() + ")" );
				return false;
			}
			if (scanID!=null && scanID.trim().length()>0 && imageSession.getScanById(scanID)==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"ERROR:  CSV file contains data for scans not currently in the session (" + scanID + ")" );
				return false;
			}
		}
		return true;
	}


	/**
	 * Gets the overall status.
	 *
	 * @return the overall status
	 */
	private void getOverallStatus() {
		this.getResponse().setEntity(new StringRepresentation(sanityChecks.getOverall_status(),MediaType.TEXT_PLAIN));
	}

	/**
	 * Calculate overall status.
	 *
	 * @param setResponse the set response
	 * @return the string
	 */
	private String calculateOverallStatus(boolean setResponse) {
		boolean hasAccepted=false, hasFailed=false, hasWarning=false;
		final String rtnVal;
		if (sanityChecks.getSession_status().equalsIgnoreCase(Outcome.FAILED.toString())) {
			hasFailed=true;
		} else if (sanityChecks.getSession_status().equalsIgnoreCase(Outcome.WARNING.toString())) {
			hasWarning=true;
		} else if (sanityChecks.getSession_status().equalsIgnoreCase(Outcome.ACCEPTED.toString())) {
			hasAccepted=true;
		}
		final List<SanSanitychecksScanChecksI> scanChecks = sanityChecks.getScans_scanChecks();
		for (SanSanitychecksScanChecksI sc : scanChecks) {
			if (sc.getStatus().equalsIgnoreCase(Outcome.FAILED.toString())) {
				if (!getScanQuality(sc.getScanId()).equalsIgnoreCase(SanityCheckUtils.QUALITY_UNUSABLE)) {
					hasFailed=true;
				}
			} else if (sc.getStatus().equalsIgnoreCase(Outcome.WARNING.toString())) {
				if (!getScanQuality(sc.getScanId()).equalsIgnoreCase(SanityCheckUtils.QUALITY_UNUSABLE)) {
					hasWarning=true;
				}
			} else if (sc.getStatus().equalsIgnoreCase(Outcome.ACCEPTED.toString())) {
				if (!getScanQuality(sc.getScanId()).equalsIgnoreCase(SanityCheckUtils.QUALITY_UNUSABLE)) {
					hasAccepted=true;
				}
			}
		}
		if (hasFailed) {
			rtnVal = Outcome.FAILED.toString();
		} else if (hasWarning) {
			rtnVal = Outcome.WARNING.toString();
		} else if (hasAccepted) {
			rtnVal = Outcome.ACCEPTED.toString();
		} else {
			rtnVal = Outcome.PASSED.toString();
		}
		if (setResponse) {
			this.getResponse().setEntity(new StringRepresentation(rtnVal,MediaType.TEXT_PLAIN));
		}
		return rtnVal;
	}
	
	/**
	 * Gets the scan quality.
	 *
	 * @param scanId the scan id
	 * @return the scan quality
	 */
	private String getScanQuality(String scanId) {
		final XnatImagescandata scan = imageSession.getScanById(scanId);
		if (scan!=null) {
			final String quality = scan.getQuality();
			if (quality!=null) {
				return quality;
			}
		}
		return "";
	}

	/**
	 * Update overall status do save.
	 */
	private void updateOverallStatusDoSave() {
		updateOverallStatus();
		saveChanges(sanityChecks);
	}
	
	/**
	 * Update overall status.
	 */
	private void updateOverallStatus() {
		sanityChecks.setOverall_status(calculateOverallStatus(false));
	}
	
	/**
	 * Accept scan checks.
	 *
	 * @param scanA the scan a
	 */
	@SuppressWarnings("deprecation")
	public void acceptScanChecks(String[] scanA) {
		if (!hasQueryVariable("scanList")) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Must specify one or more scans (scanList query variable).");
			return;
		}
		final List<SanSanitychecksScanChecksI> scanChecks = sanityChecks.getScans_scanChecks();
		final List<SanSanitychecksScanChecksI> modScanChecks = validateScanListAndReturnTBMList(scanA,scanChecks);
		if (modScanChecks==null) {
			return;
		}
		final Date modTime = new Date();
		for (SanSanitychecksScanChecksI sc : modScanChecks) {
			sc.setStatus(Outcome.ACCEPTED.toString());
			List<SanSanitychecksScanChecksCheckI> checks = sc.getCheck();
			// Keep track of which checks have been accepted
			for (SanSanitychecksScanChecksCheckI check : checks) {
				if (check.getStatus().equals(Outcome.FAILED.toString()) || check.getStatus().equals(Outcome.WARNING.toString())) {
					check.setStatus(Outcome.ACCEPTED.toString());
				}
			}
			sc.setStatusoverride_username(getUser().getLogin());
			sc.setStatusoverride_datetime(modTime);
			if (hasQueryVariable("justification")) {
				try {
					sc.setStatusoverride_justification(URLDecoder.decode(getQueryVariable("justification"),"UTF-8"));
				} catch (UnsupportedEncodingException e) {
					sc.setStatusoverride_justification(URLDecoder.decode(getQueryVariable("justification")));
				}
			}
		}
		saveChanges(sanityChecks);
	}
	
	/**
	 * Accept session checks.
	 */
	@SuppressWarnings("deprecation")
	public void acceptSessionChecks() {
		final String sessionStatus = sanityChecks.getSession_status();
		if (sessionStatus==null || !(sessionStatus.equalsIgnoreCase(Outcome.FAILED.toString()) || sessionStatus.equalsIgnoreCase(Outcome.WARNING.toString()))) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Session-level current status is not " + Outcome.FAILED.toString() +
				 " or " + Outcome.WARNING.toString() + ".");
			return;
			
		}
		final Date modTime = new Date();
		sanityChecks.setSession_status(Outcome.ACCEPTED.toString());
		// Keep track of which checks have been accepted
		List<SanSanitychecksCheckI> checks = sanityChecks.getSession_sessionChecks_check();
		for (SanSanitychecksCheckI check : checks) {
			if (check.getStatus().equals(Outcome.FAILED.toString()) || check.getStatus().equals(Outcome.WARNING.toString())) {
				check.setStatus(Outcome.ACCEPTED.toString());
			}
		}
		sanityChecks.setSession_statusoverride_username(getUser().getLogin());
		sanityChecks.setSession_statusoverride_datetime(modTime);
		if (hasQueryVariable("justification")) {
			sanityChecks.setSession_statusoverride_justification(getQueryVariable("justification"));
			try {
				sanityChecks.setSession_statusoverride_justification(URLDecoder.decode(getQueryVariable("justification"),"UTF-8"));
			} catch (UnsupportedEncodingException e) {
				sanityChecks.setSession_statusoverride_justification(URLDecoder.decode(getQueryVariable("justification")));
			}
		}
		saveChanges(sanityChecks);
	}
	
	/**
	 * Save changes.
	 *
	 * @param exp the exp
	 */
	private void saveChanges(XnatExperimentdata exp) {
		if (exp instanceof SanSanitychecks) {
			updateOverallStatus();
		} else {
			updateOverallStatusDoSave();
		}
		final PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(getUser(), exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(exp,getUser(),false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
				getResponse().setStatus(Status.SUCCESS_OK);
				return;
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
		} catch (Exception e) {
			// Do nothing for now
		}
		getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Could not save changes");
	}

	/**
	 * Validate scan list and return tbm list.
	 *
	 * @param scanA the scan a
	 * @param scanChecks the scan checks
	 * @return the list
	 */
	private List<SanSanitychecksScanChecksI> validateScanListAndReturnTBMList(String[] scanA, List<SanSanitychecksScanChecksI> scanChecks) {
		final List<SanSanitychecksScanChecksI> modList = new ArrayList<SanSanitychecksScanChecksI>();
		for (final String scanID : scanA) {
			final SanSanitychecksScanChecksI sc = getScanChecks(scanChecks,scanID);
			if (sc==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans do not exist in assessor.");
				return null;
			}
			if (sc.getStatus()==null || !(sc.getStatus().equalsIgnoreCase(Outcome.FAILED.toString()) || sc.getStatus().equalsIgnoreCase(Outcome.WARNING.toString()))) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans current status is not " + Outcome.FAILED.toString() +
					" or " + Outcome.WARNING.toString() + ".");
				return null;
			}
			modList.add(sc);
		}
		return modList;
	}

	/**
	 * Validate image scan list and return tbm list.
	 *
	 * @param scanA the scan a
	 * @param scanChecks the scan checks
	 * @return the list
	 */
	private List<XnatImagescandata> validateImageScanListAndReturnTBMList(String[] scanA, List<XnatImagescandata> scanChecks) {
		final List<XnatImagescandata> modList = new ArrayList<XnatImagescandata>();
		for (final String scanID : scanA) {
			final XnatImagescandata sc = imageSession.getScanById(scanID);
			if (sc==null) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans do not exist in session.");
				return null;
			}
			if (sc.getQuality()==null || sc.getQuality().equalsIgnoreCase(SanityCheckUtils.QUALITY_UNUSABLE)) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  One or more specified scans current quality already " + SanityCheckUtils.QUALITY_UNUSABLE + ".");
				return null;
			}
			modList.add(sc);
		}
		return modList;
	}

	/**
	 * Gets the scan checks.
	 *
	 * @param scanChecks the scan checks
	 * @param scanID the scan id
	 * @return the scan checks
	 */
	private SanSanitychecksScanChecksI getScanChecks(List<SanSanitychecksScanChecksI> scanChecks, String scanID) {
		for (final SanSanitychecksScanChecksI sc : scanChecks) {
			if (sc.getScanId().equals(scanID)) {
				return sc;
			}
		}
		return null;
	}

	/**
	 * Mark scans unusable.
	 *
	 * @param scanA the scan a
	 */
	public void markScansUnusable(String[] scanA) {
		if (!hasQueryVariable("scanList")) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Must specify one or more scans (scanList query variable).");
			return;
		}
		final List<XnatImagescandata> scans = imageSession.getScans_scan();
		final List<XnatImagescandata> modScans = validateImageScanListAndReturnTBMList(scanA,scans);
		if (modScans==null) {
			return;
		}
		for (XnatImagescandata sc : modScans) {
			final SanSanitychecksScanChecksI scanCheck = getScanChecks(sanityChecks.getScans_scanChecks(),sc.getId());
			if (scanCheck==null) {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Could not determine scan status in assessor");
				return;
			}
			if (!(scanCheck.getStatus().equalsIgnoreCase(Outcome.FAILED.toString()) || scanCheck.getStatus().equalsIgnoreCase(Outcome.WARNING.toString()))) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  May only set usability for scans marked " + Outcome.FAILED.toString() +
					 " or " + Outcome.WARNING.toString() + " in sanity checks assessor.");
				return;
			}
			sc.setQuality(SanityCheckUtils.QUALITY_UNUSABLE);
			if (sc.getNote()==null || !sc.getNote().contains("SanityChecks")) {
				sc.setNote("Set to unusable by " + getUser().getLogin() + " via SanityChecks interface.");
			}
		}
		saveChanges(imageSession);
	}

}

	
